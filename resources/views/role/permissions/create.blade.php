@extends('layout.master')
@section('content')

<div class="container">

    <div class="card">
        <div class="card-header h3">
            เพิ่มรายการ : CREATE
        </div>

        <div class="card-body">
        <form action=" " method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}  

        <h3>ค้นหา</h3>
        <div class="row"> {{-- row form --}}        
            <div class="col-md-6 col-sm-12">                                                        
                {{-- หน่วยงาน --}}            
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">คณะ/สำนัก/สถาบัน</span>
                    </div>
                    <select name="faculty" class="form-control">
                        <option value=""></option>                    
                        @foreach ($faculty as $arrFaculty)
                            <option value="{{ $arrFaculty->faculty_id }}">{{ $arrFaculty->faculty_name_th }}</option>                                                
                        @endforeach
                    </select>
                </div>            
            </div> {{-- <div class="mt-2"> --}} 

            <div class="col-md-6 col-sm-12">                                                        
                {{-- division --}}            
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">สาขา/งาน</span>
                    </div>
                    <select name="faculty" class="form-control">
                        <option value=""></option>                    
                        @foreach ($faculty as $arrFaculty)
                            <option value="{{ $arrFaculty->faculty_id }}">{{ $arrFaculty->faculty_name_th }}</option>                                                
                        @endforeach
                    </select>
                </div>            
            </div> {{-- <div class="mt-2"> --}}   

            <div class="col-md-6 col-sm-12">                                                        
                {{-- ชื่อ - สกุล --}}            
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">ชื่อ - สกุล</span>
                    </div>
                    <input type="text" class="form-control" name="code" value="" aria-describedby="basic-addon1">
                </div>                
            </div> {{-- <div class="mt-2"> --}}

        </div>

        <div class="row">
            <button type="submit" class="btn btn-info m-3">ค้นหา</button>            
        </div> {{-- row buttom --}}

        </form> {{-- row form --}}
        </div>{{-- <div class="card-body"> --}}
    </div>

    <div class="card mt-2 mb-2">
        <div class="card-body">
            <table class="table table-hover table-responsive">
                <thead>
                    <tr class="card_bg_topic">
                        <th>#</th>                            
                        <th width="25%">ชื่อ - นามสกุล</th>
                        <th width="25%">หน่วยงาน</th>
                        <th width="50%">กลุ่มผู้ใช้งาน</th>                                                                                                                                       
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>#</td>
                        <td>#</td>
                        <td>#</td>
                        <td>#</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>

@endsection