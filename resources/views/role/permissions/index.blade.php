@extends('layout.master')
@section('content')

<div class="container">

    <div class="card">
        <div class="card-header h3">กำหนดสิทธิ์ ให้ผู้ใช้งาน</div>

        <div class="card-body">
               
            <button class=" btn btn-success" data-toggle="modal" data-target="#exampleModal"> ADD LIST</button>   
            
            {{-- modal permiss --}}
            <form action="{{ route('permis.store') }}" method="POST" >
            {{ csrf_field() }}  
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">กำหนดสิทธิ์ ผู้ใช้งาน</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                            <section>
                                <b>:: เลือกชื่อ ::</b>
                                <select name="person_id" id="" class="form-control">
                                    @foreach ($User as $user_lise)
                                        <option value="{{ $user_lise->id_card }}">{{ $user_lise->name }}</option>
                                    @endforeach
                                </select>
                            </section>
    
                            <section class="mt-2">
                                <b>:: กำหนดสิทธิ์ ::</b>
                                <select name="role_id" id="" class="form-control">
                                    @foreach ($roles as $roles_list)
                                        <option value="{{ $roles_list->id }}">{{ $roles_list->name }}</option>
                                    @endforeach
                                </select>
                            </section>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                      </div>
                    </div>
                </div>                
            </form>
            {{-- modal permiss --}}
        
        
            <div class="row"> {{-- row form --}}
                <div class="mt-2 col-12">
                    
                    <table class="table table-hover table-responsive">
                        <thead>
                            <tr>
                                <th>#</th>                            
                                <th width="25%">ผู้ใช้งาน</th>
                                <th width="25%">ชื่อ - นามสกุล</th>
                                <th width="50%">กลุ่มผู้ใช้งาน</th>                                
                                <th>แก้ไข</th>                                                                               
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($permis as $arrPermis)                                                                                              
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $arrPermis->username }}</td>
                                    <td>{{ $arrPermis->name }} </td>
                                    <td>                                        
                                        @foreach ($arrPermis->permissions as $item)
                                            <span class="badge badge-primary">{{ $item->name }}</span>
                                        @endforeach                                         
                                    </td>                                    
                                    <td>
                                        <h3>                                            
                                            <a href="{{ url('manage/permis/') }}" class="btn btn-circle btn-warning btn-sm"><i class="fas fa-edit"></i></a>
                                        </h3>    
                                    </td>                                    
                                </tr>                                                      
                            @endforeach
                        </tbody>
                    </table>

                </div> {{-- <div class="mt-2"> --}}
            </div>                                         
        
            
        </div>{{-- <div class="card-body"> --}}
    </div>

</div>

@endsection