@extends('layout.master')
@section('content')

<div class="container">

    <div class="card">
        <div class="card-header h3">กำหนดสิทธิ์ ของกลุ่มผู้ใช้งาน</div>

        <div class="card-body">
           
        <a href=" {{ route('group.create') }} ">
            <button class=" btn btn-success"> ADD LIST</button>
        </a>

        <form action=""  enctype="multipart/form-data">
        {{ csrf_field() }}                           

            <div class="row"> {{-- row form --}}
                <div class="mt-2 col-12">
                    
                    <table class="table table-hover table-responsive">
                        <thead>
                            <tr>
                                <th>#</th>                            
                                <th width="50%">ชื่อกลุ่มสิทธิ์</th>
                                <th width="50%">เมนูที่ใช้ได้</th>                                
                                <th>แก้ไข</th>                        
                                <th>สถานะ</th>                        
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($roles as $arrRoles)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td> {{ $arrRoles->name }} </td>
                                <td>                                     
                                    @php
                                       $roles_menu = json_decode($arrRoles->can_access);                                       
                                    @endphp
                                    @foreach ($roles_menu as $role_id)         
                                        @php
                                            $menu_name = getMenuName($role_id);    
                                            #print $menu_name;                                        
                                        @endphp                  
                                        @foreach ($menu_name as $menu_item)
                                            <span class="badge badge-primary">{{ $menu_item->menu_name_th }}</span> 
                                        @endforeach                                                        
                                    @endforeach                                                                                                                                              
                                </td>
                                <td>
                                    <a href="" class="btn btn-circle btn-warning btn-sm"><i class="fas fa-edit"></i></a>
                                </td>
                                <td>
                                    <div class="custom-control custom-switch">
                                        <form action="" method="post">
                                            {{ csrf_field() }}
                                                @if ($arrRoles->record_status == 'D')
                                                    <input type="hidden" name="id" value="{{$arrRoles->id}}">
                                                    <input type="hidden" name="RECORD_STATUS" value="N">
                                                    <input type="checkbox" class="custom-control-input" id="{{$arrRoles->id}}"  onchange="submit()">
                                                @else
                                                    <input type="hidden" name="id" value="{{$arrRoles->id}}">
                                                    <input type="hidden" name="RECORD_STATUS" value="D">
                                                    <input type="checkbox" class="custom-control-input" id="{{$arrRoles->id}}" checked="" onchange="submit()">                                
                                                @endif
                                                    <label class="custom-control-label" for="{{$arrRoles->id}}">ออนไลน์</label>                                    
                                        </form> 
                                    </div>                                                                       
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div> {{-- <div class="mt-2"> --}}
            </div>
                       
           

        </form> {{-- row form --}}
        

            
        </div>{{-- <div class="card-body"> --}}
    </div>

</div>

@endsection