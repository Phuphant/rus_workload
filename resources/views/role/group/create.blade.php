@extends('layout.master')
@section('content')

<div class="container">

    <div class="card">
        <div class="card-header h3">
            เพิ่มรายการ : CREATE
        </div>

        <div class="card-body">
        <form action=" {{ route('group.store') }} " method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}       

        <div class="row"> {{-- row form --}}
            <div class="mt-2 col-12">
                                            

                {{-- name TH --}}            
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">ชื่อกลุ่มสิทธิ์</span>
                    </div>
                    <input type="text" class="form-control" name="name" value="" aria-describedby="basic-addon1">
                </div>            
                    

                {{-- code --}}            
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">รหัสเรียกกลุ่มสิทธิ์</span>
                    </div>
                    <input type="text" class="form-control" name="code" value="" aria-describedby="basic-addon1">
                </div>


                {{-- role --}}            
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">เมนูที่ใช้ได้</span>
                    </div>                   
                    @foreach ($menu as $list_menu)
                        <div class="form-check form-check-inline ml-2"> 
                            <input class="form-check-input" name="item_menu[]" type="checkbox" id="inlineCheckbox1" value="{{ $list_menu->id }}">
                            <label class="form-check-label" for="inlineCheckbox1">{{ $list_menu->menu_name_th }}</label>
                        </div>                                            
                    @endforeach
                </div>

                {{-- level --}}            
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">ระดับการเข้าถึง</span>
                    </div>
                    <select name="level" class="form-control">
                        <option value="user">ผู้ใช้งานทั่วไป</option>
                        <option value="staff">เจ้าหน้าที่</option>
                    </select>
                </div>

                {{-- status --}}            
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">สถานะ (Status)</span>
                    </div>
                    <div class="form-check ml-2">
                        <input class="form-check-input" type="radio" name="RECORD_STATUS" id="exampleRadios1" value="N" checked>
                        <label class="form-check-label" for="exampleRadios1">Online</label>
                    </div> 
                    <div class="form-check ml-2">
                        <input class="form-check-input" type="radio" name="RECORD_STATUS" id="exampleRadios1" value="D" >
                        <label class="form-check-label" for="exampleRadios1">Offline</label>
                    </div>                        
                </div>
            </div> {{-- <div class="mt-2"> --}}
        </div>

        <div class="row">
            <button type="submit" class="btn btn-info m-3">SAVE</button>
            <a href="" class="m-3"><button type="button" class="btn btn-secondary">RESET</button></a> 
        </div> {{-- row buttom --}}

        </form> {{-- row form --}}
        </div>{{-- <div class="card-body"> --}}
    </div>

</div>

@endsection