@extends('layout.master')
@section('content')

@if( session('alert'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>{{ session('alert')}}</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<div class="container">

    <div class="card">
        <div class="card-header h3">
            บัญชีธนาคารของคุณ 
        </div>
        <div class="card-body">
            xxx-xxxxx-x
        </div>
    </div>

</div>

@endsection