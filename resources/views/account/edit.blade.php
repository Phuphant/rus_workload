@extends('layout.master')
@section('content')

<div class="container">

    <div class="card">
        <div class="card-header h3">
            แก้ไขรายการ : EDIT
        </div>

        <div class="card-body">
        
        @foreach ($parent as $arrParent)

        <form action="{{ url('main/parent/'.$arrParent->id) }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        {{ method_field('PATCH') }}

            <div class="row"> {{-- row form --}}
                <div class="mt-2 col-12">            
                    
                    {{-- name TH --}}            
                    <div class="input-group mb-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">ชื่อเรียก ภาษาไทย (Name thai)</span>
                        </div>
                        <input type="text" class="form-control" name="PARENT_NAME_TH" value="{{ $arrParent->PARENT_NAME_TH }}" placeholder="parent name THAI"  aria-describedby="basic-addon1">
                    </div>            
        
                    {{-- name EN --}}            
                    <div class="input-group mb-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">ชื่อเรียก ภาษาอังกฤษ (Name eng)</span>
                        </div>
                        <input type="text" class="form-control" name="PARENT_NAME_EN" value="{{ $arrParent->PARENT_NAME_EN }}" placeholder="parent name ENGLISH"  aria-describedby="basic-addon1">
                    </div>

                    {{-- desc --}}            
                    <div class="input-group mb-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">รายละเอียดเพิ่มเติม (Description)</span>
                        </div>                    
                        <textarea name="PARENT_DETAIL" class="form-control" cols="30" rows="5">{{ $arrParent->PARENT_DETAIL }}</textarea>
                    </div>

                    {{-- status --}}            
                    <div class="input-group mb-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">สถานะ (Status)</span>
                        </div>
                        @if ($arrParent->RECORD_STATUS=="N")
                            @php
                                $check_N = "checked";
                                $check_D = "";
                            @endphp
                        @else
                            @php
                                $check_N = "";
                                $check_D = "checked";
                            @endphp
                        @endif

                        <div class="form-check ml-2">
                            <input class="form-check-input" type="radio" name="RECORD_STATUS" id="exampleRadios1" {{ $check_N }} value="N">
                            <label class="form-check-label" for="exampleRadios1">Online</label>
                        </div> 
                        <div class="form-check ml-2">
                            <input class="form-check-input" type="radio" name="RECORD_STATUS" id="exampleRadios1" {{ $check_D }} value="D" >
                            <label class="form-check-label" for="exampleRadios1">Offline</label>
                        </div>                        

                    </div>
                </div> {{-- <div class="mt-2"> --}}
            </div>

            <div class="row">
                <button type="submit" class="btn btn-info m-3">SAVE</button>
                <a href="" class="m-3"><button type="button" class="btn btn-secondary">RESET</button></a> 
            </div> {{-- row buttom --}}
        </form>

        @endforeach

        </div>{{-- <div class="card-body"> --}}
    </div>

</div>

@endsection