@extends('layout.master')
@section('content')

@if( session('alert'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>{{ session('alert')}}</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<div class="container">
    <div class="card">
        <div class="card-header h3">
            ฐานข้อมูลเงินเดือน ของบุคลากร ( Base Salary  )
        </div>
        {{--         
        <div class="card-body">                
            จำนวน อัตราที่มีคนรอง {{ $sly_rate }} อัตรา            
        </div>    
        --}}
        
        <div class="card-body mt-2">
            <form action="{{ route('search_rate') }}" method="post"> {{-- form search --}}
            {{ csrf_field() }}
                {{-- row --}}            
                <div class="row">
                    <div class="col-6">
                    {{-- 1 --}}            
                        {{-- faculty --}}            
                        <div class="input-group mb-1">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">หน่วยงาน : </span>
                            </div>
                            <select class="form-control" name="faculty" >
                                <option value="">:: เลือก ::</option>         
                                @foreach ($faculty as $arrFaculty)
                                    <option value="{{ $arrFaculty->faculty_id }}" {{ ($arrFaculty->faculty_id == $chk_faculty ? "selected":"" ) }} >
                                        {{ $arrFaculty->faculty_name_th }}
                                    </option>
                                @endforeach                                 
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                    {{-- 2 --}}            
                    {{-- faculty --}}            
                        <div class="input-group mb-1">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">ศูนย์พื้นที่ :  </span>
                            </div>
                            <select class="form-control" name="campus" {{ (session('role_id')=="2") ? "disabled":"" }} >
                                <option value="">:: เลือก ::</option>         
                                @foreach ($campus as $arrCampus)
                                    <option value="{{ $arrCampus->campus_id }}" {{ ($arrCampus->campus_id == $chk_campus ? "selected":"" ) }}>
                                        {{ $arrCampus->campus_name_th }}
                                    </option>
                                @endforeach                                 
                            </select>
                        </div>
                    </div>                
                    </div>

                    {{-- row --}}            
                    <div class="row">
                        <div class="col-6">
                        {{-- 1 --}}            
                            {{-- workline --}}            
                            <div class="input-group mb-1">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">สายงาน : </span>
                                </div>
                                <select class="form-control" name="workline" >
                                    <option value="">:: เลือก ::</option>         
                                    @foreach ($workline as $arrWorkline)
                                        <option value="{{ $arrWorkline->workline_id }}" {{ ($arrWorkline->workline_id == $chk_workline ? "selected":"" ) }}>
                                            {{ $arrWorkline->workline_name_th }}
                                        </option>
                                    @endforeach                                 
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                        {{-- 2 --}}            
                            
                        </div>                
                    </div>

                    {{-- name --}}   
                    <div class="row">
                        <div class="col-6">
                            <div class="input-group mb-1">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">ชื่อ : </span>
                                </div>
                                <input type="text" class="form-control" name="fname_search" value="" disabled>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-1">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">นามสกุล : </span>
                                </div>
                                <input type="text" class="form-control" name="lname_search" value="" disabled>
                            </div>
                        </div>
                    </div>                             
                    <div class="input-group mt-2 mb-2">
                        <div class="input-group-append">
                            <button class="btn btn-info" type="submit">SEARCH</button>
                        </div>
                    </div>           
                </div>           
            </form>{{-- form search --}}
        </div> {{-- <div class="card-body mt-2"> --}}
        
        <div class="card-body mt-2">
            จำนวน {{ $count_search }} รายการ
            <table class="table table-bordered table-hover">
                <thead>
                    <tr class="">
                        <th>#</th>                        
                        <th width="10%">เลขที่อัตรา</th>                        
                        <th width="20%">ชิ้อ-สกุล</th>
                        <th width="20%">ประเภท</th>
                        <th width="30%">สังกัด</th>                        
                        <th width="20%">เงินเดือนปัจจุบัน</th>                        
                        <th width="20%">แก้ไข</th>                        
                    </tr>
                </thead>
                <tbody>
                    @foreach ($search as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->rate_id }}</td>
                        <td>{{ $item->fname_th."  ".$item->lname_th }}</td>
                        <td>{{ $item->pos_name_th }}<br>{{ $item->per_type_name_th.$item->per_class_name_th }}</td>
                        <td>{{ $item->faculty_name_th." ".$item->division_name_th." ".$item->campus_name_th }} </td>
                        <td> 
                            @foreach ($item->inc_rate_salary as $item_salary)
                                {{ $item_salary->current_salary }}
                            @endforeach
                        </td>
                        <td><a href="" class="btn btn-circle btn-warning btn-sm"><i class="fas fa-edit"></i></a></td>
                    </tr>
                    @endforeach                    
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection