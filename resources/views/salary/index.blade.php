@extends('layout.master')
@section('content')

@if( session('alert'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>{{ session('alert')}}</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<div class="container">
    <div class="card">
        <div class="card-header h3">
            จัดการข้อมูลรายได้ ของบุคลากร ( Income Management )
        </div>
        <div class="card-body">                

            <div class="row"> {{-- search --}}                
            <form action="{{ route('search') }}" method="post"> {{-- form search --}}
            {{ csrf_field() }}
            
                {{-- row --}}            
                <div class="row">
                    <div class="col-6">
                        {{-- 1 --}}            
                        {{-- faculty --}}            
                        <div class="input-group mb-1">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">หน่วยงาน : </span>
                            </div>
                            <select class="form-control" name="faculty" >
                                <option value="">:: เลือก ::</option>         
                                @foreach ($faculty as $arrFaculty)
                                    <option value="{{ $arrFaculty->faculty_id }}"  >{{ $arrFaculty->faculty_name_th }}</option>
                                @endforeach                                 
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                        {{-- 2 --}}            
                        {{-- faculty --}}            
                        <div class="input-group mb-1">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">ศูนย์พื้นที่ : </span>
                            </div>
                            <select class="form-control" name="campus" >
                                <option value="">:: เลือก ::</option>         
                                @foreach ($campus as $arrCampus)
                                    <option value="{{ $arrCampus->campus_id }}"  >{{ $arrCampus->campus_name_th }}</option>
                                @endforeach                                 
                            </select>
                        </div>
                    </div>                
                </div>

                {{-- row --}}            
                <div class="row">
                    <div class="col-6">
                        {{-- 1 --}}            
                        {{-- workline --}}            
                        <div class="input-group mb-1">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">สายงาน : </span>
                            </div>
                            <select class="form-control" name="workline" >
                                <option value="">:: เลือก ::</option>         
                                @foreach ($workline as $arrWorkline)
                                    <option value="{{ $arrWorkline->workline_id }}  ">{{ $arrWorkline->workline_name_th }}</option>
                                @endforeach                                 
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                        {{-- 2 --}}            
                        
                    </div>                
                </div>
    
                {{-- name --}}            
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">ชื่อ - สกุล : </span>
                    </div>
                    <input type="text" class="form-control" name="name_search" value="" disabled aria-describedby="basic-addon1">
                </div>
    
                <div class="input-group mt-2 mb-2">
                    <div class="input-group-append">
                      <button class="btn btn-info" type="submit">SEARCH</button>
                    </div>
                </div>           
                                  
            </form>{{-- form search --}}
        </div> {{-- search --}}
        
        <div class="row d-flex flex-row-reverse"> {{-- add list --}}
            <a href="{{ route('salary.create') }}">
                <button type="button" class="btn btn-success">ADD LIST</button>
            </a>                
        </div>{{-- add list --}}

            <table class="table table-hover">
                <thead>
                    <tr class="card_bg_topic">
                        <th width="5%">#</th>
                        <th width="10%"></th>
                        <th width="40%">name</th>
                        <th width="13%">campus</th>
                        <th width="13%">status</th>
                        <th width="13%">income</th>
                        <th width="5%">manage</th>
                        <th width="5%">print</th>
                    </tr>
                </thead>
                
                <tbody>

                    @if(isset($search))
                        {{-- $search --}}                        
                        @foreach ($search as $key=>$arrSearch)
                            <tr>
                                <td>{{ $key+1 }}</td> 
                                <td>
                                    <img src="https://hrd.rmutsb.ac.th/upload/his/person/photo/{{ $arrSearch->person_picture }}" width="100%" alt="">
                                </td> 
                                <td>
                                    ชื่อ : {{ $arrSearch->fname_th }} {{ $arrSearch->lname_th }}<br>
                                    ตำแหน่ง : {{ $arrSearch->pos_name_th }}<br>
                                    หน่วยงาน : {{ $arrSearch->faculty_name_th }}<br>
                                    ประเภท : {{ $arrSearch->per_type_name_th }}{{ $arrSearch->per_class_name_th }}<br>
                                </td> 
                                <td>{{ $arrSearch->campus_name_th }}</td> 
                                <td>{{ $arrSearch->statuslist_name }}</td> 
                                <td>xx,xxx</td>                         
                                <td>
                                    <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
                                        <i class="fas fa-cogs"></i>
                                    </button>
                                    <div class="dropdown-menu">
                                        <div class="list-group">
                                            <a href="#" class="list-group-item list-group-item-action">
                                                <div class="custom-control custom-switch">
                                                    <input type="checkbox" class="custom-control-input" id="arrAsset->id" checked="checked">
                                                    <label class="custom-control-label" for="arrAsset->id">ออนไลน์</label>                                                                                           
                                                {{-- 
                                                <form action="{{ route('salary_change_status') }}" method="post">
                                                {{ csrf_field() }}
                                                    @if ($arrAsset->RECORD_STATUS == 'D')
                                                        <input type="hidden" name="id" value="arrAsset->id">
                                                        <input type="hidden" name="RECORD_STATUS" value="N">
                                                        <input type="checkbox" class="custom-control-input" id="arrAsset->id"  onchange="submit()">
                                                    @else
                                                        <input type="hidden" name="id" value="arrAsset->id">
                                                        <input type="hidden" name="RECORD_STATUS" value="D">
                                                        <input type="checkbox" class="custom-control-input" id="arrAsset->id" checked="" onchange="submit()">                                
                                                    @endif
                                                    <label class="custom-control-label" for="arrAsset->id">ออนไลน์</label>                                                                                            
                                                </form>    
                                                --}}
                                                </div>
                                            </a>
                                            <a href="{{ url('fin/salary/edit/') }}" class="btn btn-sm list-group-item list-group-item-action"><i class="fas fa-edit text-warning"></i> แก้ไขข้อมูล</a>
                                            <a href="#" class="btn btn-sm list-group-item list-group-item-action" data-toggle="modal" data-target="#myModalarrAsset->id"><i class="far fa-trash-alt text-danger"></i> ลบ</a>                                
                                        </div>                                                                                                                   
                                    </div>
                                </td>                         
                                <td><a href="" class="btn btn-circle btn-primary btn-sm"><i class="fas fa-print"></i></a></td>
                            </tr>                        
                        @endforeach                                              
                    @else
                        null
                    @endif                    
                </tbody>                                        
            </table>            
        </div>
    </div>

</div>

@endsection