@extends('layout.master')
@section('content')

<div class="container">

    <div class="card">
        <div class="card-header h3">
            จัดการเมนูระบบ ( Menu Management )
        </div>
        <div class="card-body">
        <a href="">
            <button type="button" class="btn btn-success">ADD LIST</button>
        </a>                     
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>name</th>
                        <th>menu in</th>
                        <th>menu traget</th>
                        <th>status</th>
                        <th>edit</th>
                        <th>delete</th>

                    </tr>
                </thead>
                
                <tbody>
                @foreach ($MenuContent as $arrMenuContent)
                @if ($arrMenuContent->record_status == "D")
                    <tr class="table-danger">  
                @else
                    <tr class="table-secondary">  
                @endif                                  
                        <td>{{ $arrMenuContent->menu_id }}</td>
                        <td>
                            <b>{{ $arrMenuContent->menu_name_th }}</b> [ code : {{ $arrMenuContent->menu_code }} ]                                                         
                        </td>
                        <td>{{ $arrMenuContent->menu_in }}</td>
                        <td>{{ $arrMenuContent->menu_target }}</td>
                        <td><center>{{ $arrMenuContent->record_status }}</center></td>
                        <td><a href=""><i class="fas fa-edit"></i></a></td>
                        <td><a href=""><i class="far fa-trash-alt"></a></i></td>
                    </tr>
                    @foreach ($SubMenuContent as $arrSubMenuContent)
                    @if ($arrSubMenuContent->menu_in == $arrMenuContent->menu_id)
                        @if ($arrSubMenuContent->record_status == "D")
                            <tr class="table-danger">  
                        @else
                            <tr >  
                        @endif                                           
                                <td>{{ $arrSubMenuContent->menu_id }}</td>
                                <td>{{ $arrSubMenuContent->menu_name_th }}</td>
                                <td>{{ $arrMenuContent->menu_code }}</td>
                                <td>{{ $arrSubMenuContent->menu_target }}</td>                                
                                <td><center>{{ $arrSubMenuContent->record_status }}</center></td>
                                <td><a href=""><i class="fas fa-edit"></i></a></td>
                                <td><a href=""><i class="far fa-trash-alt"></a></i></td>
                            </tr>
                        @endif                      
                    @endforeach
                @endforeach
                </tbody>                                
            </table>

        </div>
    </div>

</div>

@endsection