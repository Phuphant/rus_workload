@extends('layout.master')
@section('content')

<div class="container">

    <div class="card">
        <div class="card-header h3">
            เพิ่มรายการ : CREATE
        </div>

        <div class="card-body">
        <form action="{{ route('menu.store') }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}       

        <div class="row"> {{-- row form --}}
            <div class="mt-2 col-12">
                            
                {{-- name TH --}}            
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">ชื่อเรียก ภาษาไทย (Name thai)</span>
                    </div>
                    <input type="text" class="form-control" name="menu_name_th" value="" placeholder="ชื่อเรียก"  aria-describedby="basic-addon1">
                </div>                 
                
                {{-- menu_in --}}            
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">เป็นเมนูย่อยของ (Sub Menu)</span>
                    </div>
                    <select name="menu_in" id="" class="form-control">
                        <option value="main">::เป็นเมนูหลัก::</option>
                        @foreach ($MenuContent as $item)
                            <option value="{{ $item->id }}">{{ $item->menu_name_th }}</option>
                        @endforeach                        
                    </select>
                </div> 

                {{-- code --}}            
                <div class="input-group mb-1">                    
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">รหัสเรียก (Code)</span>                        
                    </div>                    
                    <input type="text" class="form-control" name="menu_code" value="" >                    
                    <span class="ml-2" style="color: red;">* โปรดระบุ code กรณีที่เป็นเมนูหลัก </span>
                </div>

                {{-- order --}}            
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">ลำดับแาดงผล (Order)</span>
                    </div>                    
                    <input type="text" class="form-control" name="menu_order" value="" >                    
                </div>

                {{-- menu target --}}            
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Target (Menu target)</span>
                    </div>
                    <select name="menu_target" id="" class="form-control">
                        <option value="_self">อยู่หน้าต่างเดิม</option>
                        <option value="_blank">เปิดหน้าต่างใหม่</option>
                    </select>
                </div>

                {{-- path --}}            
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">URL (Path)</span>
                    </div>                    
                    <input type="text" class="form-control" name="menu_path" value="" >                    
                </div>
                
                {{-- status --}}            
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">สถานะ (Status)</span>
                    </div>
                    <div class="form-check ml-2">
                        <input class="form-check-input" type="radio" name="RECORD_STATUS" id="exampleRadios1" value="N" checked>
                        <label class="form-check-label" for="exampleRadios1">Online</label>
                    </div> 
                    <div class="form-check ml-2">
                        <input class="form-check-input" type="radio" name="RECORD_STATUS" id="exampleRadios1" value="D" >
                        <label class="form-check-label" for="exampleRadios1">Offline</label>
                    </div>                        
                </div>
            </div> {{-- <div class="mt-2"> --}}
        </div>

        <div class="row">
            <button type="submit" class="btn btn-info m-3">SAVE</button>
            <a href="" class="m-3"><button type="button" class="btn btn-secondary">RESET</button></a> 
        </div> {{-- row buttom --}}

        </form> {{-- row form --}}
        </div>{{-- <div class="card-body"> --}}
    </div>

</div>

@endsection