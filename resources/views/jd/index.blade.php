@extends('layout.master')
@section('content')

@if( session('alert'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>{{ session('alert')}}</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<div class="container">
    <div class="card">
        <div class="card-header h3">
            ข้อมูละคุณสมบัติมาตรฐานของตำแหน่งงาน (Job Specifications) 
        </div>
        {{--         
        <div class="card-body">                
            จำนวน อัตราที่มีคนรอง {{ $sly_rate }} อัตรา            
        </div>    
        --}}
        
        <div class="card-body mt-2">
            <form action="{{-- route('search_rate') --}}" method="post"> {{-- form search --}}
            {{ csrf_field() }}
                {{-- row --}}            
                <div class="row">
                    <div class="col-6">
                    {{-- 1 --}}            
                        {{-- faculty --}}            
                        <div class="input-group mb-1">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">หน่วยงาน : </span>
                            </div>
                            <select class="form-control" name="faculty" >
                                <option value="">:: เลือก ::</option>         
                                                              
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                    {{-- 2 --}}            
                    {{-- faculty --}}            
                        <div class="input-group mb-1">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">ศูนย์พื้นที่ :  </span>
                            </div>
                            <select class="form-control" name="campus" {{ (session('role_id')=="2") ? "disabled":"" }} >
                                <option value="">:: เลือก ::</option>         
                                                               
                            </select>
                        </div>
                    </div>                
                    </div>

                    {{-- row --}}            
                    <div class="row">
                        <div class="col-6">
                        {{-- 1 --}}            
                            {{-- workline --}}            
                            <div class="input-group mb-1">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">สายงาน : </span>
                                </div>
                                <select class="form-control" name="workline" >
                                    <option value="">:: เลือก ::</option>         
                                                                   
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                        {{-- 2 --}}            
                            
                        </div>                
                    </div>

                    {{-- name --}}   
                    <div class="row">
                        <div class="col-6">
                            <div class="input-group mb-1">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">ชื่อ : </span>
                                </div>
                                <input type="text" class="form-control" name="fname_search" value="" disabled>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="input-group mb-1">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">นามสกุล : </span>
                                </div>
                                <input type="text" class="form-control" name="lname_search" value="" disabled>
                            </div>
                        </div>
                    </div>                             
                    <div class="input-group mt-2 mb-2">
                        <div class="input-group-append">
                            <button class="btn btn-info" type="submit">SEARCH</button>
                        </div>
                    </div>           
                </div>           
            </form>{{-- form search --}}
        </div> {{-- <div class="card-body mt-2"> --}}
        
        <div class="card-body mt-2">
            ความรับผิดชอบหลัก {{ $job_count }} รายการ
            <table class="table table-bordered table-hover">
                <thead>
                    <tr class="">
                        <th>#</th>                        
                        <th width="40%">หน้าที่ความรับผิดชอบหลัก/กิจกรรมหลัก</th>                                                
                        <th width="50%">วัตถุประสงค์หลักของงาน </th>                                                                                   
                        <th width="10%">จัดการ</th>                        
                    </tr>
                </thead>
                <tbody>
                    
                    @foreach ($job->where('category_in','main') as $job_main)                    
                        <tr class="table-secondary">
                            <td>{{ $loop->iteration }}</td>
                            <td colspan="2">{{ $job_main->name_th }}</td>
                            <td><a href="" class="btn btn-circle btn-warning btn-sm"><i class="fas fa-edit"></i></a></td>
                        </tr> 
                        @foreach ($job->where('category_in',$job_main->id) as $job_sub)
                            <tr >
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $job_sub->name_th }}</td>
                                <td>{{ $job_sub->detail_th }}</td>                            
                                <td><a href="" class="btn btn-circle btn-warning btn-sm"><i class="fas fa-edit"></i></a></td>
                            </tr> 
                        @endforeach                                                         
                    @endforeach 
                                   
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection