@extends('layout.master')
@section('content')

<div class="container">

    <div class="card">
        <div class="card-header h3">
            แก้ไขรายการ : EDIT
        </div>

        <div class="card-body">
        
        

        <form action="{{ url('manage/menu/'.$Menu->id) }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        {{ method_field('PATCH') }}

        <div class="row"> {{-- row form --}}
            <div class="mt-2 col-12">
                            
                {{-- name TH --}}            
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">ชื่อเรียก ภาษาไทย (Name thai)</span>
                    </div>
                    <input type="text" class="form-control" name="menu_name_th" value="{{ $Menu->menu_name_th }}">
                </div>                 
                
                {{-- menu_in --}}            
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">เป็นเมนูย่อยของ (Sub Menu)</span>
                    </div>
                    <select name="menu_in" id="" class="form-control">                        
                        @php
                            $selected = "";
                            if( $Menu->menu_in == "main"){
                                    $selected = "selected";
                            }
                        @endphp
                            <option value="main" {{ $selected }}>::เป็นเมนูหลัก::</option>                        
                            @foreach ($MenuContent as $item)
                                <option value="{{ $item->id }}" @if($item->id==$Menu->menu_in) selected @endif>
                                    {{ $item->menu_name_th }}
                                </option>
                            @endforeach                                                
                    </select>
                </div> 

                {{-- code --}}            
                <div class="input-group mb-1">                    
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">รหัสเรียก (Code)</span>                        
                    </div>                    
                    <input type="text" class="form-control" name="menu_code" value="{{ $Menu->menu_code }}" >                    
                    <span class="ml-2" style="color: red;">* โปรดระบุ code กรณีที่เป็นเมนูหลัก </span>
                </div>

                {{-- order --}}            
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">ลำดับแาดงผล (Order)</span>
                    </div>                    
                    <input type="text" class="form-control" name="menu_order" value="{{ $Menu->menu_order }}" >                    
                </div>

                {{-- menu target --}}            
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Target (Menu target)</span>
                    </div>
                    <select name="menu_target" id="" class="form-control">
                        @if ($Menu->menu_target=="_self")
                            @php
                                $select_s = "selected";
                                $select_b = "";
                            @endphp
                        @else
                            @php
                                $select_s = "";
                                $select_b = "checked";
                        @endphp
                    @endif
                        <option value="_self" {{ $select_s }}>อยู่หน้าต่างเดิม</option>
                        <option value="_blank" {{ $select_b }}>เปิดหน้าต่างใหม่</option>
                    </select>
                </div>

                {{-- path --}}            
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">URL (Path)</span>
                    </div>                    
                    <input type="text" class="form-control" name="menu_path" value="{{ $Menu->menu_path }}" >                    
                </div>
                
                {{-- status --}}            
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">สถานะ (Status)</span>
                    </div>
                    @if ($Menu->record_status=="N")
                            @php
                                $check_N = "checked";
                                $check_D = "";
                            @endphp
                        @else
                            @php
                                $check_N = "";
                                $check_D = "checked";
                        @endphp
                    @endif
                    <div class="form-check ml-2">
                        <input class="form-check-input" type="radio" name="RECORD_STATUS" id="exampleRadios1" value="N" {{ $check_N }}>
                        <label class="form-check-label" for="exampleRadios1">Online</label>
                    </div> 
                    <div class="form-check ml-2">
                        <input class="form-check-input" type="radio" name="RECORD_STATUS" id="exampleRadios1" value="D" {{ $check_D }} >
                        <label class="form-check-label" for="exampleRadios1">Offline</label>
                    </div>                        
                </div>
            </div> {{-- <div class="mt-2"> --}}
        </div>

            <div class="row">
                <button type="submit" class="btn btn-info m-3">SAVE</button>
                <a href="" class="m-3"><button type="button" class="btn btn-secondary">RESET</button></a> 
            </div> {{-- row buttom --}}
        </form>

        

        </div>{{-- <div class="card-body"> --}}
    </div>

</div>

@endsection