@extends('layout.master')
@section('content')

<div class="container">

    <div class="card">
        <div class="card-header h3">
            จัดการรายการครุภัณฑ์/พัสดุ ( Asset Item Management )
        </div>
        <div class="card-body">
        <a href="">
            <button type="button" class="btn btn-success">ADD LIST</button>
        </a>      
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>name</th>
                        <th>status</th>
                        <th>edit</th>
                        <th>delete</th>
                    </tr>
                </thead>
                
                <tbody>
                @foreach ($asset as $arrAsset)               
                @if ($arrAsset->RECORD_STATUS == "D")
                    <tr class="table-danger">  
                @else
                    <tr>
                @endif                 
                        <td>{{ $arrAsset->ASSET_ID }}</td>
                        <td><b>{{ $arrAsset->ASSET_NAME_TH }}</b></td>
                        <td><center>{{ $arrAsset->RECORD_STATUS }}</center></td>
                        <td><a href=""><i class="fas fa-edit"></i></a></td>
                        <td><a href=""><i class="far fa-trash-alt"></a></i></td>
                    </tr>                    
                @endforeach
                </tbody>       
                {{ $asset->render() }}                         
            </table>

        </div>
    </div>

</div>

@endsection