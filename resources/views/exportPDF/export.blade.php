<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>RUS_STATEMENT | ระบบฐานข้อมูลเงินเดือน</title>

    <!-- Custom fonts for this template-->
    <link href="{{ asset('theme/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    
    <link rel="stylesheet" href="https://site-assets.fontawesome.com/releases/v6.0.0/css/all.css">


    <!-- Custom styles for this template-->
    <link href="{{ asset('theme/css/sb-admin-2.min.css') }}" rel="stylesheet">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Sarabun:wght@100;200;400&display=swap" rel="stylesheet">
    
    <style>
		body {
		  font-family: 'Sarabun', sans-serif;
          font-weight: 400;
		}
		h3 {
		  font-family: 'Sarabun', sans-serif;
          font-weight:bold;
		}
    </style>

</head>

<body id="page-top">

    <table class="table table-bordered">
        {{-- HEAD TB --}}
        <tr>
            <td colspan="2" class="text-center slipfont">
                <img src="{{ asset('images/logo_rmutsb.gif') }}" class="mb-2" width="60"><br>
                <h3>มหาวิทยาลัยเทคโนโลยีราชมงคลสุวรรณภูมิ <br>
                    ใบแจ้งรายการเงินเดือน</h3>
                <br>
                <div class="row justify-content-between">
                    <div class="col-6"><b>ชื่อ-สกุล : </b>{{ Auth::user()->vw_his_person->getFullName() }}</div>
                    <div class="col-6"><b>เลขที่อัตรา : </b>{{ Auth::user()->vw_his_person->rate_id }}</div>
                </div> 
                <div class="row justify-content-between">
                    <div class="col-6"><b>สังกัด : </b>{{ Auth::user()->vw_his_person->faculty_name_th }}</div>
                    <div class="col-6"><b>ศูนย์พื้นที่ : </b>{{ Auth::user()->vw_his_person->campus_name_th }}</div>
                </div> 
                <div class="row justify-content-between">
                    <div class="col-6"><b>ประจำเดิอน-ปี : </b>{{ $sys_month->sys_month_th }} {{ $sys_year }}</div>
                    <div class="col-6">
                        <button class="btn btn-info" onclick="window.print()"><i class="fas fa-print"></i> Print </button><br>
                        วันที่พิมพ์รายการ {{ $cur_date }} เวลา {{ $cur_time }}
                    </div>                    
                </div>                              
            </td>            
        </tr>
        <tr>
            <td colspan="2">
                {{-- <a class="btn btn-primary" href="{{ URL::to('/print') }}">PRINT</a>  --}}
            </td>
        </tr>
        <tr>
            <td><b>รายการ</b></td>
            <td><b>จำนวนเงิน</b></td>
        </tr>

        {{-- รายรับ - INCOME --}}
        <tr class="bg-primary text-white"><td colspan="2"><b>รายการรายรับ</b></td></tr>
        @foreach ($list_inc as $inc)
            @php
                $field_inc = $inc->item_code;   
                if ($inc_net->$field_inc <> "0") {                    
                    $arrINC[$field_inc] = $inc_net->$field_inc;                                            
                }                                                               
            @endphp                                       
        @endforeach          
        @foreach ($arrINC as $key_inc=>$inc)
            <tr>
                <td>
                    @foreach ($list_inc->where('item_code',$key_inc) as $arrIncome)
                        {{ $arrIncome->item_name_th }}
                    @endforeach
                </td>
                <td>
                    @php
                        print number_format($inc,2,'.',',')." บาท";
                    @endphp 
                </td>
            </tr>            
        @endforeach
        <tr>
            <td class="text-right"><b>รวมรายรับ</b></td>
            <td>                
                @php
                    print number_format($inc_net->inc_summary,2,'.',',')." บาท";
                @endphp                 
            </td>
        </tr>
        {{-- รายรับ - INCOME --}}  


        {{-- รายจ่าย - EXPEND --}}
        <tr class="bg-primary text-white"><td colspan="2"><b>รายการรายจ่าย</b></td></tr>
        @foreach ($list_exp as $exp)
            @php
                $field_exp = $exp->item_code;   
                if ($inc_net->$field_exp <> "0") {
                    #$filed_val = $inc_net->$field_exp;
                    #print $field_exp."->".$inc_net->$field_exp;
                    $arrEXP[$field_exp] = $inc_net->$field_exp;                                            
                }                                                               
            @endphp                                       
        @endforeach          
        @foreach ($arrEXP as $key=>$exp)
            <tr>
                <td>
                    @foreach ($list_exp->where('item_code',$key) as $arrExpend)
                        {{ $arrExpend->item_name_th }}
                    @endforeach
                </td>
                <td>
                    @php
                        print number_format($exp,2,'.',',')." บาท";
                    @endphp                 
                </td>
            </tr>            
        @endforeach
        <tr>
            <td class="text-right"><b>รวมรายจ่าย</b></td>
            <td>
                @php
                    print number_format($inc_net->exp_summary,2,'.',',')." บาท";
                @endphp             
            </td>
        </tr>
        {{-- รายจ่าย - EXPEND --}}

        <tr class="bg-primary text-white"><td colspan="2"><b>คงเหลือรายรับสิทธิ</b></td></tr>   
        <tr>
            <td></td>
            <td>
                @php
                    print number_format($inc_net->inc_net,2,'.',',')." บาท";                    
                @endphp
            </td>
        </tr>
    </table>

</body>

</html>