@extends('layout.master')
@section('content')

@if( session('alert'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>{{ session('alert')}}</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<div class="container">

    <div class="card">
        <div class="card-header h3">
            จัดการเมนูระบบ ( Menu Management )
        </div>
        <div class="card-body">
        <a href="{{ route('menu.create') }}">
            <button type="button" class="btn btn-success mb-2" >ADD LIST</button>
        </a>                     
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>name</th>
                        <th>menu in</th>
                        <th>menu traget</th>
                        <th>status</th>
                        <th>edit</th>
                        <th>delete</th>
                    </tr>
                </thead>
                
                <tbody>
                @foreach ($MenuContent as $arrMenuContent)
                @if ($arrMenuContent->record_status == "D")
                    <tr class="table-danger">  
                @else
                    <tr class="table-secondary">  
                @endif                                  
                        <td>{{ $loop->iteration }}</td>
                        <td>
                            <b>{{ $arrMenuContent->menu_name_th }}</b> [ code : {{ $arrMenuContent->menu_code }} ]                                                         
                        </td>
                        <td>{{ $arrMenuContent->menu_in }}</td>
                        <td>{{ $arrMenuContent->menu_target }}</td>
                        <td>
                            <div class="custom-control custom-switch">                            
                                <form action="{{ route('menu_change_status') }}" method="post">
                                {{ csrf_field() }}
                                    @if ($arrMenuContent->record_status == 'D')
                                        <input type="hidden" name="id" value="{{$arrMenuContent->id}}">
                                        <input type="hidden" name="RECORD_STATUS" value="N">
                                        <input type="checkbox" class="custom-control-input" id="{{$arrMenuContent->id}}"  onchange="submit()">
                                    @else
                                        <input type="hidden" name="id" value="{{$arrMenuContent->id}}">
                                        <input type="hidden" name="RECORD_STATUS" value="D">
                                        <input type="checkbox" class="custom-control-input" id="{{$arrMenuContent->id}}" checked="" onchange="submit()">                                
                                    @endif
                                        <label class="custom-control-label" for="{{$arrMenuContent->id}}">ออนไลน์</label>
                                    
                                </form>
                            </div>
                        </td>
                        <td><a href="{{ url('manage/menu/edit/'.$arrMenuContent->id) }}" class="btn btn-circle btn-warning btn-sm"><i class="fas fa-edit"></i></a></td>
                        <td>
                            <a href="" class="btn btn-circle btn-danger btn-sm" data-toggle="modal" data-target="#myModal{{$arrMenuContent->id}}"><i class="far fa-trash-alt"></i></a>
                            <!-- Modal -->
                            <div class="modal fade" id="myModal{{$arrMenuContent->id}}" role="dialog">
                                <div class="modal-dialog">                                
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">          
                                    <h4 class="modal-title text-danger"><b>ยืนยันการลบข้อมูล</b></h4>
                                    </div>
                                    <div class="modal-body">                      
                                        <p>ยืนยันการลบข้อมูล <b>{{ $arrMenuContent->menu_name_th }}</b> โดย <b> -user- </b> ซึ่งเมื่อลบแล้วข้อมูลจะไม่ปรากฎในระบบอีก กรุณาตรวจสอบความถูกต้องก่อนการลบ</p>
                                        <p><i>กรณีต้องการกู้คือกรุณาติดต่อผู้ดูแลระบบ</i></p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <form action="{{ url('manage/menu/'.$arrMenuContent->id) }}" method="post">
                                        {{ csrf_field() }}      
                                        {{ method_field('DELETE') }}                                  
                                            <input type="hidden" name="id" value="{{$arrMenuContent->id}}">
                                            <button type="button" class="btn btn-danger"  onclick="submit()">COMFIRM DELETE</button>
                                        </form>
                                    </div>
                                </div>                                
                                </div>
                            </div>
                        </td>
                    </tr>
                    @foreach ($SubMenuContent as $arrSubMenuContent)
                    @if ($arrSubMenuContent->menu_in == $arrMenuContent->id)
                        @if ($arrSubMenuContent->record_status == "D")
                            <tr class="table-danger">  
                        @else
                            <tr >  
                        @endif                                           
                                <td>-</td>
                                <td>{{ $arrSubMenuContent->menu_name_th }}</td>
                                <td>{{ $arrMenuContent->menu_code }}</td>
                                <td>{{ $arrSubMenuContent->menu_target }}</td>                                
                                <td>
                                    <div class="custom-control custom-switch">                            
                                        <form action="{{ route('menu_change_status') }}" method="post">
                                        {{ csrf_field() }}
                                            @if ($arrSubMenuContent->record_status == 'D')
                                                <input type="hidden" name="id" value="{{$arrSubMenuContent->id}}">
                                                <input type="hidden" name="RECORD_STATUS" value="N">
                                                <input type="checkbox" class="custom-control-input" id="{{$arrSubMenuContent->id}}"  onchange="submit()">
                                            @else
                                                <input type="hidden" name="id" value="{{$arrSubMenuContent->id}}">
                                                <input type="hidden" name="RECORD_STATUS" value="D">
                                                <input type="checkbox" class="custom-control-input" id="{{$arrSubMenuContent->id}}" checked="" onchange="submit()">                                
                                            @endif
                                                <label class="custom-control-label" for="{{$arrSubMenuContent->id}}">ออนไลน์</label>
                                            
                                        </form>
                                    </div>
                                </td>
                                <td><a href="{{ url('manage/menu/edit/'.$arrSubMenuContent->id) }}" class="btn btn-circle btn-warning btn-sm"><i class="fas fa-edit"></i></a></td>
                                <td>
                                    <a href="" class="btn btn-circle btn-danger btn-sm" data-toggle="modal" data-target="#myModal{{$arrSubMenuContent->id}}"><i class="far fa-trash-alt"></i></a>
                                    <!-- Modal -->
                                    <div class="modal fade" id="myModal{{$arrSubMenuContent->id}}" role="dialog">
                                        <div class="modal-dialog">                                
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">          
                                            <h4 class="modal-title text-danger"><b>ยืนยันการลบข้อมูล</b></h4>
                                            </div>
                                            <div class="modal-body">                      
                                                <p>ยืนยันการลบข้อมูล <b>{{ $arrSubMenuContent->menu_name_th }}</b> โดย <b> -user- </b> ซึ่งเมื่อลบแล้วข้อมูลจะไม่ปรากฎในระบบอีก กรุณาตรวจสอบความถูกต้องก่อนการลบ</p>
                                                <p><i>กรณีต้องการกู้คือกรุณาติดต่อผู้ดูแลระบบ</i></p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <form action="{{ url('manage/menu/'.$arrSubMenuContent->id) }}" method="post">
                                                {{ csrf_field() }}      
                                                {{ method_field('DELETE') }}                                  
                                                    <input type="hidden" name="id" value="{{$arrSubMenuContent->id}}">
                                                    <button type="button" class="btn btn-danger"  onclick="submit()">COMFIRM DELETE</button>
                                                </form>
                                            </div>
                                        </div>                                
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endif                      
                    @endforeach
                @endforeach
                </tbody>                                
            </table>

        </div>
    </div>

</div>

@endsection