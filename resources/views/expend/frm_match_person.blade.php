@extends('layout.master')
@section('content')

<div class="container">

    <div class="card">
        <div class="card-header h3">กำหนดรายการ รายจ่ายให้บุคลากร</div>

        <div class="card-body">

            <div class="row"> {{-- search --}}                
                <form action="{{ url('fin/match_per_expend/') }}"  method="post" > {{-- form search --}}
                {{ csrf_field() }}

                    {{-- row --}}            
                    <div class="row">                        
                        <div class="col-12">
                            {{-- 1 --}}            
                            {{-- faculty --}}            
                            <div class="input-group mb-1">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">ศูนย์พื้นที่ : </span>
                                </div>
                                <select class="form-control" name="campus" >
                                    <option value="">:: เลือก ::</option>         
                                    @foreach ($campus as $arrCampus)
                                        <option value="{{ $arrCampus->campus_id }}" >{{ $arrCampus->campus_name_th }}</option>
                                    @endforeach                                 
                                </select>
                            </div>
                        </div>                
                    </div>                    
        
                    <div class="input-group mt-2 mb-2">
                        <div class="input-group-append">
                          <button class="btn btn-info" type="submit"><i class="fas fa-search"></i> SEARCH</button>
                        </div>
                    </div>           
                                      
                </form>{{-- form search --}}
            </div> {{-- search --}}

        <form action=""  enctype="multipart/form-data">
        {{ csrf_field() }}                           

            <div class="row"> {{-- row form --}}
                <div class="mt-2 col-12">
                    
                    <table class="table table-hover">
                        <thead>
                            <tr class="card_bg_topic">
                                <th width="5%">#</th>                            
                                <th width="30%">ชื่อ - นามสกุล</th>
                                <th width="30%">หน่วยงาน</th>                                
                                <th width="15%" class="text-center">กำหนดรายจ่าย</th>                        
                                <th width="25%" class="text-center">สถานะ</th>                        
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($per_faculty as $arrPerFac)
                                @foreach ($arrPerFac->vw_his_person->where('statuslist',9) as $person)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $person->person_id }} - {{ $person->getFullName() }}</td>
                                    <td>{{ $person->faculty_name_th }}</td>
                                    <td class="text-center">
                                        <a href="{{ url('fin/match_per_expend_step2/'.$person->person_id) }}" class="btn btn-sm btn-primary"><i class="fas fa-plus"></i></a>
                                    </td>
                                    <td class="text-center">                                        
                                        @foreach ($per_expend as $item)
                                            @if($item->person_id == $person->person_id)
                                                <font style="color: red">กำหนดรายการรายจ่ายแล้ว</font>
                                            @endif
                                        @endforeach                                        
                                    </td>
                                </tr>                                
                                @endforeach                        
                            @endforeach                        
                        </tbody>
                    </table>

                </div> {{-- <div class="mt-2"> --}}
            </div>
           
            <div class="row">{{-- row buttom --}}
                <button type="submit" class="btn btn-info m-3">NEXT</button>                
                <button type="reset" class="btn btn-secondary m-3">RESET</button>
            </div> {{-- row buttom --}}
           

        </form> {{-- row form --}}
        

            
        </div>{{-- <div class="card-body"> --}}
    </div>

</div>

@endsection