@extends('layout.master')
@section('content')

<div class="container">

    <div class="card">
        <div class="card-header h3">
            แก้ไขรายการ : EDIT
        </div>

        <div class="card-body">
        
        @foreach ($expend as $arrExpend)

        <form action="{{ url('main/expend/'.$arrExpend->id) }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        {{ method_field('PATCH') }}

            <div class="row"> {{-- row form --}}
                <div class="mt-2 col-12">
                                                
                    {{-- name TH --}}            
                    <div class="input-group mb-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">ชื่อเรียก ภาษาไทย (Name thai)</span>
                        </div>
                        <input type="text" class="form-control" name="item_name_th" value="{{ $arrExpend->item_name_th }}" aria-describedby="basic-addon1">
                    </div>            
        
                    {{-- name EN --}}            
                    <div class="input-group mb-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">ชื่อเรียก ภาษาอังกฤษ (Name eng)</span>
                        </div>
                        <input type="text" class="form-control" name="item_name_en" value="{{ $arrExpend->item_name_en }}" aria-describedby="basic-addon1">
                    </div>

                    {{-- campus --}}                      
                    <div class="input-group mb-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">ศูนย์พื้นที่ (Chart)</span>
                        </div>
                        <div class="ml-2">                                                                        
                            @foreach ($campus as $arrCampus)                            
                                <div class="form-check form-check-inline ml-2">         
                                    @if(strpos($arrExpend->item_campus,$arrCampus->campus_id))
                                        @php
                                            $check = "checked";
                                        @endphp
                                    @else
                                        @php
                                            $check = " ";
                                        @endphp
                                    @endif                                
                                    <input class="form-check-input" {{ $check }} name="item_campus[]" type="checkbox" id="inlineCheckbox1" value="{{ $arrCampus->campus_id }}">
                                    <label class="form-check-label" for="inlineCheckbox1">{{ $arrCampus->campus_name_th }}</label>
                                </div>
                            @endforeach                        
                        </div>
                    </div>

                    {{-- price --}}            
                    <div class="input-group mb-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">จำนวนเงิน (บาท)</span>
                        </div>
                        <input type="text" class="form-control" name="item_price" value="{{ $arrExpend->item_price }}" aria-describedby="basic-addon1">
                    </div>

                    {{-- rate --}}            
                    <div class="input-group mb-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">ร้อยละ (%)</span>
                        </div>
                        <input type="text" class="form-control" name="item_rate" value="{{ $arrExpend->item_rate }}" aria-describedby="basic-addon1">
                    </div>
                    
                    {{-- status --}}            
                    <div class="input-group mb-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">สถานะ (Status)</span>
                        </div>
                        @if ($arrExpend->RECORD_STATUS=="N")
                            @php
                                $check_N = "checked";
                                $check_D = "";
                            @endphp
                        @else
                            @php
                                $check_N = "";
                                $check_D = "checked";
                            @endphp
                        @endif
                        <div class="form-check ml-2">
                            <input class="form-check-input" type="radio" name="RECORD_STATUS" id="exampleRadios1" value="N" {{ $check_N }}>
                            <label class="form-check-label" for="exampleRadios1">Online</label>
                        </div> 
                        <div class="form-check ml-2">
                            <input class="form-check-input" type="radio" name="RECORD_STATUS" id="exampleRadios1" value="D" {{ $check_D }}>
                            <label class="form-check-label" for="exampleRadios1">Offline</label>
                        </div>                        
                    </div>
                </div> {{-- <div class="mt-2"> --}}
            </div>

            <div class="row">
                <button type="submit" class="btn btn-info m-3">SAVE</button>
                <a href="" class="m-3"><button type="button" class="btn btn-secondary">RESET</button></a> 
            </div> {{-- row buttom --}}
        </form>

        @endforeach

        </div>{{-- <div class="card-body"> --}}
    </div>

</div>

@endsection