@extends('layout.master')
@section('content')

@if( session('alert'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>{{ session('alert')}}</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<div class="container">

    <div class="card">
        <div class="card-header h3">
            กำหนดรายจ่ายของบุคลากร ตามประเภทรายจ่าย ( Assign Expend Management )
        </div>
        <div class="card-body">        

        <div class="row"> {{-- search --}}
            <div class="input-group mb-3 mt-2">
                <input type="text" class="form-control" placeholder="search content" >
                <div class="input-group-append">
                  <button class="btn btn-info" type="button">SEARCH</button>
                </div>
            </div>                           
        </div> {{-- search --}}
        
        {{-- 
        <div class="row d-flex flex-row-reverse"> {{-- add list }}
            <a href="{{ route('expend.create') }}">
                <button type="button" class="btn btn-success">ADD LIST</button>
            </a>                
        </div>{{-- add list }}    
        --}}

            <table class="table table-hover">
                <thead>
                    <tr class="card_bg_topic">
                        <th>#</th>
                        <th></th>
                        <th>name</th>
                        <th>status</th>
                        <th class="text-center">กำหนดรายชื่อ</th>                        
                    </tr>
                </thead>
                
                <tbody>
                @foreach ($expend as $key=>$arrExpend)               
                @if ($arrExpend->RECORD_STATUS == "D")
                    <tr class="table-danger">  
                @else
                    <tr>
                @endif                 
                        <td>{{ $key+1 }}</td>
                        <td>-</td>
                        <td>{{ $arrExpend->item_name_th }}</td>
                        <td>
                            <div class="custom-control custom-switch">                                                        
                                @if ($arrExpend->RECORD_STATUS == 'D')
                                    <input type="checkbox" class="custom-control-input" disabled >
                                @else                                    
                                    <input type="checkbox" class="custom-control-input" disabled checked="">                                
                                @endif
                                    <label class="custom-control-label" for="{{$arrExpend->id}}">ออนไลน์</label>                                
                            </div>
                        </td>
                        <td class="text-center">                            
                            <a href="{{ url('fin/assign_person/'.$arrExpend->id) }}" class="btn btn-sm btn-primary"><i class="fas fa-plus"></i></a>
                        </td>                        
                    </tr>                    
                @endforeach
                </tbody>                                
            </table>        
        </div>
    </div>

</div>

@endsection