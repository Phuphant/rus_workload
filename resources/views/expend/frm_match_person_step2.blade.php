@extends('layout.master')
@section('content')

@if( session('alert'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>{{ session('alert')}}</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<div class="container">

    <div class="card">
        <div class="card-header h3">กำหนดรายการ รายจ่ายให้บุคลากร ของ 
            @foreach ($person as $dataPerson)
                {{ $dataPerson->getFullName() }}
            @endforeach
        </div>

        <div class="card-body">

        <form action="{{ url('fin/match_per_expend_step2') }}" method="POST">
        {{ csrf_field() }}                           
        <input type="hidden" name="person_id" value="{{ $id }}">
            <div class="row"> {{-- search --}}                
                <table class="table table-hover">
                    <thead>
                        <tr class="card_bg_topic">
                            <th>#</th>
                            <th>รายการรายจ่าย</th>
                            <th>เลือก</th>                            
                        </tr>
                    </thead>
                    <tbody>   
                        @foreach ($expend as $arrExpend)
                            <tr>
                                <td>{{ $loop->index+1 }}</td>
                                <td>{{ $arrExpend->item_name_th }}</td>
                                <td>
                                    <input type="checkbox" id="myCheck" value="{{ $arrExpend->id }}" name="chk_expend[]">
                                </td>                                
                            </tr>        
                        @endforeach                                                
                    </tbody>
                </table>
            </div> {{-- search --}}    
            <div class="row">{{-- row buttom --}}
                <button type="submit" class="btn btn-info m-3">NEXT</button>                
                <button type="reset" class="btn btn-secondary m-3">CANCEL</button>
            </div> {{-- row buttom --}}            
        </form>

        </div>{{-- <div class="card-body"> --}}
    </div>

</div>

@endsection