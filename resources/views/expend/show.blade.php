@extends('layout.master')
@section('content')


<div class="container">

    <div class="card">
        <div class="card-header h3">
            <h3><i class="fa-solid fa-money-bill-1-wave"></i> รายการรายจ่าย ( Expend )</h3>
        </div>
        <div class="card-body"> 
            
            {{-- row1 --}}
            {{-- $select_M --}}
            {{-- $select_Y --}}
            <div class="row">
                <div class="col-4">
                    <select class="form-control" name="txtMonth" onchange="window.location.replace('{{ url('expend') }}/'+this.value)">
                        @foreach ($sys_month as $arrMonth)
                            <option value="{{ $arrMonth->id }}" {{ ($arrMonth->id==$select_M)?'selected':'' }}>{{ $arrMonth->sys_month_th }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-4">
                    <select class="form-control" name="txtYear" disabled>
                        @foreach ($sys_year as $arrYear)
                            <option value=" {{ $arrYear->id }} ">{{ $arrYear->sys_year_th }}</option>
                        @endforeach
                    </select>    
                </div>                
            </div>

            {{-- row profile --}}
            <section class="mt-2">                
                <div class="row">
                    <table class="table border-primary">
                        <thead>
                            <tr class="text-white bg-primary">
                                <th>
                                    ข้อมูลผู้ใช้งาน
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <b>{{ $his_person->getfullname() }}</b>
                                    ( {{ $his_person->per_type_name_th}}{{ $his_person->per_class_name_th}} )<br> 
                                    ตำแหน่ง : {{ $his_person->pos_name_th}} <br>
                                    หน่วยงาน : {{ $his_person->faculty_name_th}}<br>
                                    ศูนย์พื้นที่ : {{ $his_person->campus_name_th}}<br>                                    
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </section>

            {{-- row table detail --}}
            {{-- session('camp_id') --}}
            <section class="mt-2">
                <div class="row">
                    <table class="table table-bordered">
                        <thead>
                            <tr class="text-white bg-primary">                                
                                <th>ลำดับ</th>
                                <th>รายการ</th>
                                <th>จำนวนเงิน</th>                                
                            </tr>
                        </thead>                                                    
                        {{-- $select_M --}}
                        {{-- @php
                            $income2 = $income->where('slip_month',$select_M);
                            //print_r($income2);
                        @endphp
                        @if(isset($income2))  
                        --}}
                        @foreach ($slip_item->where('item_type','expend')  as $arrItem)    
                            @php
                                $item = json_decode($arrItem->item_campus);
                                $key = array_search(session('camp_id'),$item);                                   
                                #print $key;                        
                            @endphp                                           
                            <tr>                                                            
                                <td> {{ $loop->iteration }}</td>
                                <td>                                    
                                    {{ $arrItem->item_name_th }}
                                </td>
                                <td> 
                                    @php
                                        $field = $arrItem->item_code
                                    @endphp
                                    @foreach ($expend as $arrExpend)                                        
                                        @php
                                            print number_format($arrExpend->$field,2,'.',',')." บาท";
                                            $exp_summary = $arrExpend->exp_summary;
                                        @endphp
                                    @endforeach
                                </td>
                            </tr>                                                                              
                        </tbody>                        
                        @endforeach  
                        <tfoot>
                            <tr>
                                <td><b>รวมรายจ่ายประจำเดือน</b></td>
                                <td colspan="2"><b> 
                                    @if(isset($exp_summary))   
                                        @php
                                            print number_format($exp_summary,2,'.',',')." บาท";
                                        @endphp 
                                    @endif
                                </b></td>
                            </tr>
                        </tfoot>
                        {{-- @endif  --}}
                    </table>
                </div>    
            </section>   

        </div>
    </div>

</div>

@endsection