@extends('layout.master')
@section('content')

@if( session('alert'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>{{ session('alert')}}</strong>
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
    </button>
</div>
@endif

<div class="container">

    <div class="card">
        <div class="card-header h3">
            จัดการรายการรายจ่าย ( Expend Management )
        </div>
        <div class="card-body">        

        <div class="row"> {{-- search --}}
            <div class="input-group mb-3 mt-2">
                <input type="text" class="form-control" placeholder="search content" >
                <div class="input-group-append">
                  <button class="btn btn-info" type="button">SEARCH</button>
                </div>
            </div>                           
        </div> {{-- search --}}
        
        <div class="row d-flex flex-row-reverse"> {{-- add list --}}
            <a href="{{ route('expend.create') }}">
                <button type="button" class="btn btn-success">ADD LIST</button>
            </a>                
        </div>{{-- add list --}}
        {{ session('camp_id') }}
            <table class="table table-hover">
                <thead>
                    <tr class="card_bg_topic">
                        <th>#</th>
                        <th>name</th>
                        <th>campus</th>
                        <th>status</th>
                        <th>edit</th>
                        <th>delete</th>
                    </tr>
                </thead>
                
                <tbody>
                @foreach ($expend as $arrExpend)               
                @if ($arrExpend->RECORD_STATUS == "D")
                    <tr class="table-danger">  
                @else
                    <tr>
                @endif                 
                        <td>{{ $arrExpend->id }}</td>
                        <td>{{ $arrExpend->item_name_th }}</td>
                        <td>{{ $arrExpend->item_campus }}</td>
                        <td>
                            <div class="custom-control custom-switch">                            
                            <form action="{{ route('expend_change_status') }}" method="post">
                            {{ csrf_field() }}
                                @if ($arrExpend->RECORD_STATUS == 'D')
                                    <input type="hidden" name="id" value="{{$arrExpend->id}}">
                                    <input type="hidden" name="RECORD_STATUS" value="N">
                                    <input type="checkbox" class="custom-control-input" id="{{$arrExpend->id}}"  onchange="submit()">
                                @else
                                    <input type="hidden" name="id" value="{{$arrExpend->id}}">
                                    <input type="hidden" name="RECORD_STATUS" value="D">
                                    <input type="checkbox" class="custom-control-input" id="{{$arrExpend->id}}" checked="" onchange="submit()">                                
                                @endif
                                    <label class="custom-control-label" for="{{$arrExpend->id}}">ออนไลน์</label>
                                
                            </form>
                            </div>
                        </td>
                        <td><a href="{{ url('main/expend/edit/'.$arrExpend->id) }}" class="btn btn-circle btn-warning btn-sm"><i class="fas fa-edit"></i></a></td>
                        <td>
                            <a href="" class="btn btn-circle btn-danger btn-sm" data-toggle="modal" data-target="#myModal{{$arrExpend->id}}"><i class="far fa-trash-alt"></i></a>
                            <!-- Modal -->
                            <div class="modal fade" id="myModal{{$arrExpend->id}}" role="dialog">
                                <div class="modal-dialog">                                
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">          
                                    <h4 class="modal-title text-danger"><b>ยืนยันการลบข้อมูล</b></h4>
                                    </div>
                                    <div class="modal-body">                      
                                        <p>ยืนยันการลบข้อมูล <b>{{ $arrExpend->item_name_th }}</b> โดย <b> -user- </b> ซึ่งเมื่อลบแล้วข้อมูลจะไม่ปรากฎในระบบอีก กรุณาตรวจสอบความถูกต้องก่อนการลบ</p>
                                        <p><i>กรณีต้องการกู้คือกรุณาติดต่อผู้ดูแลระบบ</i></p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <form action="{{ url('main/expend/'.$arrExpend->id) }}" method="post">
                                        {{ csrf_field() }}      
                                        {{ method_field('DELETE') }}                                  
                                            <input type="hidden" name="id" value="{{$arrExpend->id}}">
                                            <button type="button" class="btn btn-danger"  onclick="submit()">COMFIRM DELETE</button>
                                        </form>
                                    </div>
                                </div>                                
                                </div>
                            </div>
                        </td>
                    </tr>                    
                @endforeach
                </tbody>                                
            </table>
            {{ $expend->render() }}
        </div>
    </div>

</div>

@endsection