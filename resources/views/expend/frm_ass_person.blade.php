@extends('layout.master')
@section('content')

<div class="container">

    <div class="card">
        <div class="card-header h3">
            @foreach ($expend as $arrExpend)
                กำหนดรายจ่ายให้บุคลากร -> <b>{{ $arrExpend->item_name_th }}</b>
                @php
                  $exp_id =  $arrExpend->id;
                @endphp
            @endforeach
            ประจำเดือน xxxx ปี xxxx            
        </div>

        <div class="card-body">        
            {{-- 
            <div class="row"> {{-- search - -}}                
                <form action="{{ url('fin/assign_person/'.$exp_id) }}" method="post"> {{-- form search - -}}
                {{ csrf_field() }}

                    {{-- row -- }}            
                    <div class="row">                        
                        <div class="col-12">
                            {{-- 1 - -}}            
                            {{-- faculty - -}}                              
                            <div class="input-group mb-1">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">ศูนย์พื้นที่ : </span>
                                </div>
                                <select class="form-control" name="campus" >
                                    <option value="">:: เลือก ::</option>         
                                    @foreach ($campus as $arrCampus)
                                        <option value="{{ $arrCampus->campus_id }}" {{ $arrCampus->campus_id==$camp_id ? 'selected':'' }} >{{ $arrCampus->campus_name_th }}</option>
                                    @endforeach                                 
                                </select>
                            </div>    
                        </div>                
                    </div>                    
        
                    <div class="input-group mt-2 mb-2">
                        <div class="input-group-append">
                          <button class="btn btn-info" type="submit"><i class="fas fa-search"></i> SEARCH</button>
                        </div>
                    </div>           
                                      
                </form>{{-- form search - -}}
            </div> {{-- search -}}
            --}}
        
            <div class="row"> {{-- search --}}                
                <form action="{{ route('search') }}" method="post"> {{-- form search --}}
                {{ csrf_field() }}
                
                    {{-- row --}}            
                    <div class="row">
                        <div class="col-6">
                            {{-- 1 --}}            
                            {{-- faculty --}}            
                            <div class="input-group mb-1">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">หน่วยงาน : </span>
                                </div>
                                <select class="form-control" name="faculty" >
                                    <option value="">:: เลือก ::</option>         
                                    @foreach ($faculty as $arrFaculty)
                                        <option value="{{ $arrFaculty->faculty_id }}"  >{{ $arrFaculty->faculty_name_th }}</option>
                                    @endforeach                                 
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            {{-- 2 --}}            
                            {{-- faculty --}}            
                            <div class="input-group mb-1">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">ศูนย์พื้นที่ : </span>
                                </div>
                                <select class="form-control" name="campus" >
                                    <option value="">:: เลือก ::</option>         
                                    @foreach ($campus as $arrCampus)
                                        <option value="{{ $arrCampus->campus_id }}"  >{{ $arrCampus->campus_name_th }}</option>
                                    @endforeach                                 
                                </select>
                            </div>
                        </div>                
                    </div>
    
                    {{-- row --}}            
                    <div class="row">
                        <div class="col-6">
                            {{-- 1 --}}            
                            {{-- workline --}}            
                            <div class="input-group mb-1">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">สายงาน : </span>
                                </div>
                                <select class="form-control" name="workline" >
                                    <option value="">:: เลือก ::</option>         
                                    @foreach ($workline as $arrWorkline)
                                        <option value="{{ $arrWorkline->workline_id }}  ">{{ $arrWorkline->workline_name_th }}</option>
                                    @endforeach                                 
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            {{-- 2 --}}            
                            
                        </div>                
                    </div>
        
                    {{-- name --}}            
                    <div class="input-group mb-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">ชื่อ - สกุล : </span>
                        </div>
                        <input type="text" class="form-control" name="name_search" value="" disabled aria-describedby="basic-addon1">
                    </div>
        
                    <div class="input-group mt-2 mb-2">
                        <div class="input-group-append">
                          <button class="btn btn-info" type="submit">SEARCH</button>
                        </div>
                    </div>           
                                      
                </form>{{-- form search --}}
            </div> {{-- search --}}


        <form action="{{ route('assign_expend.store') }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}       
                    

            <input type="hidden" name="exp_id" value="{{ $exp_id }}">
            <input type="hidden" name="campus_id" value="{{ $camp_id }}">
            <input type="hidden" name="exp_month" value="{{ $exp_month }}">
            <input type="hidden" name="exp_year" value="{{ $exp_year }}">

            <div class="row"> {{-- row form --}}
                <div class="mt-2 col-12">
                    
                    <table class="table table-hover">
                        <thead>
                            <tr class="card_bg_topic">
                                <th>#</th>                            
                                <th>name</th>
                                <th class="text-left">
                                    กำหนดรายชื่อ
                                    {{-- 
                                    <div class="form-check">
                                        <input type="checkbox" class="select-all checkbox" name="select-all" /> เลือกทั้งหมด                           
                                    </div> 
                                    --}}                               
                                </th>                        
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($search as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $item->vw_his_person->getfullname() }}</td>
                                <td class="text-left">
                                    <div class="row">
                                        <div class="form-check">                                                
                                            <input type="checkbox" id="myCheck" value="{{ $loop->index }}" name="chk_person[]" onclick="myFunction(this.value)">
                                            <input type="hidden" name="data[{{ $loop->index }}][person_id]" value="{{ $item->person_id }}" >
                                        </div>
                                        <div>
                                            <p id="{{ $loop->index }}" style="display:none">ระบุจำนวนเงิน : <input type="number" name="data[{{ $loop->index }}][exp_price]" id="exp_price" class="form-control ml-2"></p>
                                        </div>                                            
                                    </div>
                                </td>
                            </tr>      
                            @endforeach                                                  
                        </tbody>
                    </table>

                </div> {{-- <div class="mt-2"> --}}
            </div>
           
            <div class="row">{{-- row buttom --}}
                <button type="submit" class="btn btn-info m-3">NEXT</button>                
                <button type="reset" class="btn btn-secondary m-3">RESET</button>
            </div> {{-- row buttom --}}
           

        </form> {{-- row form --}}
        

            
        </div>{{-- <div class="card-body"> --}}
    </div>

</div>

@endsection

@section('script')

<script>
    function myFunction($id) {
    //alert($id);
      var checkBox = document.getElementById("myCheck");
      //var text = document.getElementById("text");
      /*
      if (checkBox.checked == true){
        text.style.display = "block";
      } else {
         text.style.display = "none";
      }
      */
      if (checkBox.checked == true){
            //alert($id);
            var text = document.getElementById($id);
            text.style.display = "block";        
      }else {
            //alert("-");
            text.style.display = "none";
      }
    }
    </script>
@endsection