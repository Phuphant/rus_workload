@extends('layout.master')
@section('content')

<div class="container">

    <div class="card">
        <div class="card-header h3">
            เพิ่มรายการ : CREATE
        </div>

        <div class="card-body">
        <form action="{{ route('income.store') }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}       

        <div class="row"> {{-- row form --}}
            <div class="mt-2 col-12">
                                            

                {{-- name TH --}}            
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">ชื่อเรียก ภาษาไทย (Name thai)</span>
                    </div>
                    <input type="text" class="form-control" name="item_name_th" value="" aria-describedby="basic-addon1">
                </div>            
    
                {{-- name EN --}}            
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">ชื่อเรียก ภาษาอังกฤษ (Name eng)</span>
                    </div>
                    <input type="text" class="form-control" name="item_name_en" value="" aria-describedby="basic-addon1">
                </div>

                {{-- code --}}            
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">รหัสเรียก (Code)</span>
                    </div>
                    <input type="text" class="form-control" name="item_code" value="" aria-describedby="basic-addon1">
                </div>

                {{-- campus --}}                      
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">ศูนย์พื้นที่ (Chart)</span>
                    </div>
                    <div class="ml-2">                                                                               
                        @foreach ($campus as $arrCampus)
                            <div class="form-check form-check-inline ml-2"> 
                                <input class="form-check-input" name="item_campus[]" type="checkbox" id="inlineCheckbox1" value="{{ $arrCampus->campus_id }}">
                                <label class="form-check-label" for="inlineCheckbox1">{{ $arrCampus->campus_name_th }}</label>
                            </div>
                        @endforeach                        
                    </div>
                </div>

                {{-- price --}}            
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">จำนวนเงิน (บาท)</span>
                    </div>
                    <input type="text" class="form-control" name="item_price" value="" aria-describedby="basic-addon1">
                </div>

                {{-- rate --}}            
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">ร้อยละ (%)</span>
                    </div>
                    <input type="text" class="form-control" name="item_rate" value="" aria-describedby="basic-addon1">
                </div>
                
                {{-- status --}}            
                <div class="input-group mb-1">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">สถานะ (Status)</span>
                    </div>
                    <div class="form-check ml-2">
                        <input class="form-check-input" type="radio" name="RECORD_STATUS" id="exampleRadios1" value="N" checked>
                        <label class="form-check-label" for="exampleRadios1">Online</label>
                    </div> 
                    <div class="form-check ml-2">
                        <input class="form-check-input" type="radio" name="RECORD_STATUS" id="exampleRadios1" value="D" >
                        <label class="form-check-label" for="exampleRadios1">Offline</label>
                    </div>                        
                </div>
            </div> {{-- <div class="mt-2"> --}}
        </div>

        <div class="row">
            <button type="submit" class="btn btn-info m-3">SAVE</button>
            <a href="" class="m-3"><button type="button" class="btn btn-secondary">RESET</button></a> 
        </div> {{-- row buttom --}}

        </form> {{-- row form --}}
        </div>{{-- <div class="card-body"> --}}
    </div>

</div>

@endsection