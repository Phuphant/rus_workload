@extends('layout.master')
@section('content')


<div class="container">

    <div class="card">
        <div class="card-header h3">            
            <h3><i class="fa-solid fa-wallet"></i> รายการรายรับ ( Income )</h3>
        </div>
        <div class="card-body"> 

            {{-- row1 --}}
            {{-- $select_M --}}
            {{-- $select_Y --}}
            <div class="row">
                <div class="col-4">
                    <select class="form-control" name="txtMonth" onchange="window.location.replace('{{ url('income') }}/'+this.value)">
                        @foreach ($sys_month as $arrMonth)
                            <option value="{{ $arrMonth->id }}" {{ ($arrMonth->id==$select_M)?'selected':'' }}>{{ $arrMonth->sys_month_th }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-4">
                    <select class="form-control" name="txtYear" disabled>
                        @foreach ($sys_year as $arrYear)
                            <option value=" {{ $arrYear->id }} ">{{ $arrYear->sys_year_th }}</option>
                        @endforeach
                    </select>    
                </div>                
            </div>

            {{-- row profile --}}
            <section class="mt-2">                
                <div class="row">
                    <table class="table border-primary">
                        <thead>
                            <tr class="text-white bg-primary">
                                <th>
                                    ข้อมูลผู้ใช้งาน
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <b>{{ $his_person->getfullname() }}</b>
                                    ( {{ $his_person->per_type_name_th}}{{ $his_person->per_class_name_th}} )<br> 
                                    ตำแหน่ง : {{ $his_person->pos_name_th}} <br>
                                    หน่วยงาน : {{ $his_person->faculty_name_th}}<br>
                                    ศูนย์พื้นที่ : {{ $his_person->campus_name_th}}<br>                                    
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </section>

            {{-- row table detail --}}
            <section class="mt-2">
                <div class="row">
                    <table class="table table-bordered">
                        <thead>
                            <tr class="text-white bg-primary">                                
                                <th>อัตราเงินเดือน</th>
                                <th>เงินเดือนตกเบิก</th>
                                <th>เงินประจำตำแหน่ง</th>
                                <th>รายรับอื่นๆ</th>
                            </tr>
                        </thead>                                                    
                        {{-- $select_M --}}
                        {{-- @php
                            $income2 = $income->where('slip_month',$select_M);
                            //print_r($income2);
                        @endphp
                        @if(isset($income2))  
                        --}}
                        @foreach ($income  as $arrIncome)                                               
                            <tr>                            
                                <td>
                                    {{-- $arrIncome->inc_salary --}}
                                    @php
                                        print number_format($arrIncome->inc_salary,2,'.',',')." บาท";
                                    @endphp
                                </td>
                                <td>
                                    {{-- $arrIncome->inc_salary_late --}}
                                    @php
                                        print number_format($arrIncome->inc_salary_late,2,'.',',')." บาท";
                                    @endphp
                                </td>
                                <td>      
                                    @php
                                        print "หัวหน้าสาขา : ".number_format($arrIncome->inc_pos_branch,2,'.',',')." บาท<br>";
                                        print "วิชาการ : ".number_format($arrIncome->inc_pos_acad,2,'.',',')." บาท<br>";
                                        print "บริหาร : ".number_format($arrIncome->inc_pos_exec,2,'.',',')." บาท<br>";
                                    @endphp                                                                                            
                                </td>
                                <td>
                                    @php
                                        print number_format($arrIncome->inc_other,2,'.',',')." บาท";
                                    @endphp
                                </td>
                            </tr>                                                                              
                        </tbody>
                        <tfoot>
                            <tr>
                                <td><b>รวมรายรับประจำเดือน</b></td>
                                <td colspan="4">
                                    <b>
                                    @php                                        
                                        print number_format($arrIncome->inc_summary,2,'.',',')." บาท";
                                    @endphp
                                    </b>
                                </td>
                            </tr>
                        </tfoot>
                        @endforeach  
                        {{-- @endif  --}}
                    </table>
                </div>    
            </section>   

        </div>
    </div>

</div>

@endsection