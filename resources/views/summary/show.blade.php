@extends('layout.master')
@section('content')


<div class="container">

    <div class="card">
        <div class="card-header h3">            
            <h3><i class="fa-solid fa-wallet"></i> ตรวจสอบข้อมูลสลิปเงินเดือน ( My Statment )</h3>
        </div>
        <div class="card-body"> 

            {{-- row1 --}}
            {{-- $select_M --}}
            {{-- $select_Y --}}
            <div class="row">
                <div class="col-4">
                    <select class="form-control" name="txtMonth" onchange="window.location.replace('{{ url('summary') }}/'+this.value)">
                        @foreach ($sys_month as $arrMonth)
                            <option value="{{ $arrMonth->id }}" {{ ($arrMonth->id==$select_M)?'selected':'' }}>{{ $arrMonth->sys_month_th }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-4">
                    <select class="form-control" name="txtYear" disabled>
                        @foreach ($sys_year as $arrYear)
                            <option value=" {{ $arrYear->id }} ">{{ $arrYear->sys_year_th }}</option>
                        @endforeach
                    </select>    
                </div>                
                <div class="col-4">                       
                    {{-- <button class="btn btn-secondary"><i class="fas fa-print"></i> พิมพ์ข้อมูล</button> --}}                    
                    <a class="btn btn-success" href=" {{ url('pdf/'.$select_M) }}" {{-- data-toggle="modal" data-target="#exampleModal" --}}> 
                        <i class="fas fa-print"></i> พิมพ์ข้อมูล
                    </a>                
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Messages :: </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                            <div class="modal-body text-center">
                                <h2><i class="fas fa-exclamation-triangle text-warning"></i> </h2> อยู่ระหว่างปรับปรุงระบบ ขออภัยในความไม่สะดวก
                            </div>
                            <div class="modal-footer">                                                            
                                <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                        </div>
                    </div>    
                    
                </div>
            </div>

            {{-- row profile --}}
            <section class="mt-2">                
                <div class="row">
                    <table class="table border-primary">
                        <thead>
                            <tr class="text-white bg-primary">
                                <th>
                                    ข้อมูลผู้ใช้งาน
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <b>{{ $his_person->getfullname() }}</b>
                                    ( {{ $his_person->per_type_name_th}}{{ $his_person->per_class_name_th}} )<br> 
                                    ตำแหน่ง : {{ $his_person->pos_name_th}} <br>
                                    หน่วยงาน : {{ $his_person->faculty_name_th}}<br>
                                    ศูนย์พื้นที่ : {{ $his_person->campus_name_th}}<br>                                    
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </section>

            {{-- row table detail --}}        
            
            <section class="mt-2">
                <h4 class="bg-light"> รายรับสุทธิ</h4>
                <div class="row">
                    <table class="table table-borderd">                        
                        <thead>
                            <tr class="text-white bg-primary">
                                <th>รายรับสุทธิ</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    @foreach ($inc_net as $incNet)
                                        <b>
                                            @php
                                                print number_format($incNet->inc_net,2,'.',',')." บาท";
                                            @endphp
                                        </b>
                                    @endforeach
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </section>
            
            <section class="mt-2">
                <h4 class="bg-light"> รายการรายรับ</h4>
                <div class="row">
                    <table class="table table-bordered">
                        <thead>
                            <tr class="text-white bg-primary">                                
                                <th>อัตราเงินเดือน</th>
                                <th>เงินเดือนตกเบิก</th>
                                <th>เงินประจำตำแหน่ง</th>
                                <th>รายรับอื่นๆ</th>
                            </tr>
                        </thead>                                                    
                        {{-- $select_M --}}
                        {{-- @php
                            $income2 = $income->where('slip_month',$select_M);
                            //print_r($income2);
                        @endphp
                        @if(isset($income2))  
                        --}}
                        @foreach ($inc_net  as $arrIncome)                                               
                            <tr>                            
                                <td>
                                    @php
                                        print number_format($arrIncome->inc_salary,2,'.',',')." บาท";
                                    @endphp                                                                        
                                </td>
                                <td>
                                    @php
                                        print number_format($arrIncome->inc_salary_late,2,'.',',')." บาท";
                                    @endphp                                                                         
                                </td>
                                <td>     
                                    @php
                                        print "หัวหน้าสาขา : ".number_format($arrIncome->inc_pos_branch,2,'.',',')." บาท<br>";
                                        print "วิชาการ : ".number_format($arrIncome->inc_pos_acad,2,'.',',')." บาท<br>";
                                        print "บริหาร : ".number_format($arrIncome->inc_pos_exec,2,'.',',')." บาท<br>";
                                    @endphp                                                                                             
                                </td>
                                <td>
                                    @php
                                        print number_format($arrIncome->inc_other,2,'.',',')." บาท";
                                    @endphp                                      
                                </td>
                            </tr>                                                                              
                        </tbody>
                        <tfoot>
                            <tr>
                                <td><b>รวมรายรับประจำเดือน</b></td>
                                <td colspan="4"><b>                                    
                                    @php                                        
                                        print number_format($arrIncome->inc_summary,2,'.',',')." บาท";
                                    @endphp                                    
                                    </b>
                                </td>
                            </tr>
                        </tfoot>
                        @endforeach  
                        {{-- @endif  --}}
                    </table>
                </div>    
            </section>
            
            
            <section class="mt-2">
                <h4 class="bg-light"> รายการรายจ่าย</h4>
                <div class="row">
                    <table class="table table-bordered">
                        <thead>
                            <tr class="text-white bg-primary">                                
                                <th width="20%">ลำดับ</th>
                                <th width="40%">รายการ</th>
                                <th width="40%">จำนวนเงิน</th>                                
                            </tr>
                        </thead>                                                                            
                        @foreach ($slip_item->where('item_type','expend')  as $arrItem)                                               
                            <tr>                                                            
                                <td> {{ $arrItem->id }} </td>
                                <td> {{ $arrItem->item_name_th }} </td>
                                <td> 
                                    @php
                                        $field = $arrItem->item_code
                                    @endphp                                    
                                    @foreach ($inc_net as $arrExpend)                                        
                                        @php
                                            print number_format($arrExpend->$field,2,'.',',')." บาท";
                                            $exp_summary = $arrExpend->exp_summary;
                                        @endphp
                                    @endforeach                                        
                                </td>
                            </tr>                                                                              
                        </tbody>                        
                        @endforeach  
                        <tfoot>
                            <tr>
                                <td><b>รวมรายจ่ายประจำเดือน</b></td>
                                <td colspan="2"><b>                                     
                                    @if(isset($exp_summary))   
                                        @php
                                            print number_format($exp_summary,2,'.',',')." บาท";
                                        @endphp                                                                             
                                    @endif                                    
                                </b>
                                </td>
                            </tr>
                        </tfoot>
                        {{-- @endif  --}}
                    </table>
                </div>    
            </section>

        </div>
    </div>

</div>

@endsection