@extends('layout.master')
@section('content')

<div class="container">
@php
    /*
@foreach ($his_person as $arrHisPerson)
     
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-8">
                    <h3><i class="fas fa-user-alt"></i> ข้อมูลส่วนตัวของบุคลากร</h3>
                </div>
                <div class="col-4">
                    <i class="fas fa-calendar-alt"></i> แสดงข้อมูลล่าสุดเมื่อ {{ $cur_date }}
                </div>
            </div>
        </div> {{-- header --}}
                

        <div class="card-body">            
            <div class="row">
                <div class="col-lg-3 mb-4">
                    <div class="card shadow mb-4">                        
                        <div class="card-body">
                            <img src="https://hrd.rmutsb.ac.th/upload/his/person/photo/{{ $arrHisPerson->person_picture }}" width="100%" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 mb-4">
                    <div class="card">                        
                        <div class="card-body">
                            {{-- Auth::user()->vw_his_person->person_id --}}
                            <div class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <td class="bg-light">เลขที่ตำแหน่ง</td>
                                        <td>{{ $arrHisPerson->rate_id }}</td>
                                        <td class="bg-light">ชื่อ - สกุล</td>
                                        <td>{{ $arrHisPerson->fname_th }}  {{ $arrHisPerson->lname_th }}</td>                                                            
                                    </tr>
                                    <tr>
                                        <td class="bg-light">รหัสบัตรประจำตัวประชาชน</td>
                                        <td>{{ $arrHisPerson->id_card }}</td>
                                        <td class="bg-light">ตำแหน่งงาน</td>
                                        <td>{{ $arrHisPerson->pos_name_th }}</td>
                                    </tr>
                                    <tr>
                                        <td class="bg-light">สังกัด</td>
                                        <td>
                                            {{ $arrHisPerson->faculty_name_th }}<br>
                                            {{ $arrHisPerson->unit_name_th }}
                                        </td>
                                        <td class="bg-light">ศูนย์</td>
                                        <td>{{ $arrHisPerson->campus_name_th }}</td>
                                    </tr>
                                    <tr>
                                        <td class="bg-light">ประเภทบุคลากร</td>
                                        <td>{{ $arrHisPerson->per_type_name_th }}{{ $arrHisPerson->per_class_name_th }}</td>       
                                        <td class="bg-light">ระดับ</td>
                                        <td>{{ $arrHisPerson->pos_acad_name_th }}</td>
                                    </tr>
                                    <tr>
                                        <td class="bg-light">สายงาน</td>
                                        <td>{{ $arrHisPerson->workline_name_th }}</td>
                                        <td class="bg-light">สถานะ</td>
                                        <td>{{ $arrHisPerson->statuslist_name }}</td>
                                    </tr>
                                </table>
                           </div>
                        </div>
                    </div>
                </div>
            </div> {{-- row-body --}}
        </div>{{-- body --}}
    </div> {{-- card --}}

@endforeach      
    */
@endphp
</div> {{-- container --}}


@endsection