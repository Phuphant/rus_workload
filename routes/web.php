<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/

Route::get('/','PersonController@index')->middleware('auth');
route::get('profile','PersonController@show_detail')->name('profile');
Route::get('income/{id?}','IncomeController@show');
Route::get('expend/{id?}','ExpendController@show');
Route::get('summary/{id?}','SummaryController@show');
Route::get('account','AccountController@index');

route::get('pdf/{val?}','ExportController@pdf');
route::get('print','ExportController@createPDF');

Route::get('/phpinfo',function(){
    return phpinfo();
});

Route::prefix('manage')->group(function(){
    #Route::get('/','ManageController@index')->name('manage.index');   
    Route::resource('menu','MenuController');   
    Route::get('menu/edit/{id}','MenuController@edit'); 
    Route::post('/menu_change_status','MenuController@change_status')->name('menu_change_status'); 

    Route::resource('group','RolesController');  
    Route::resource('permis','PermisController');  
    #Route::get('permis/{id?}','PermisController');  
    route::get('changepermis/{id?}','PermisController@changepermis');
});


Route::prefix('main')->group(function(){

    Route::resource('person','PersonController'); 
    Route::resource('jd','CategoryController');
    /*
    Route::resource('income','IncomeController'); 
    Route::get('income/edit/{id}','IncomeController@edit'); 
    Route::post('/income_change_status','IncomeController@change_status')->name('income_change_status');

    Route::resource('expend','ExpendController');
    Route::get('expend/edit/{id}','ExpendController@edit'); 
    Route::post('/expend_change_status','ExpendController@change_status')->name('expend_change_status');
    */
    
});

Route::prefix('fin')->group(function(){
    
    Route::resource('salary','SalaryController'); 
    Route::get('salary/edit/{id}','SalaryController@edit'); 
    Route::post('/salary_change_status','SalaryController@change_status')->name('salary_change_status');
    Route::post('/salary/search','SalaryController@search')->name('search');

    Route::resource('assign_expend','AssignExpendController'); 
    Route::post('assign_person/{id}','AssignExpendController@create_person'); 
    Route::get('assign_person/{id}','AssignExpendController@create_person');  
    Route::get('match_per_expend','AssignExpendController@match_person'); 
    Route::get('match_per_expend_step2/{id}','AssignExpendController@match_person_step2');
    Route::post('match_per_expend_step2','AssignExpendController@create_person_expend');  
    Route::get('salary_rate','SalaryController@salary_rate'); 
    Route::post('/rate/search','SalaryController@rate_search')->name('search_rate');


    
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');