<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class mas_faculty extends Model
{
    
    #protected $connection = "sqlsrv_hrd";    
    protected $table = "mas_faculty";

    public function vw_his_person()
    {
        return $this->hasMany('App\vw_his_person', 'faculty_id', 'faculty_id')->where('statuslist','9');
    }
}
