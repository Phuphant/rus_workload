<?php

namespace App\Http\Controllers;

use App\inv_parent;
use App\sys_menu;

use Illuminate\Http\Request;

class ParentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['menu'] = sys_menu::where('menu_in','main')
                        ->where('record_status','N')        
                        ->orderby('menu_order')
                        ->get();
        $data['sub_menu'] = sys_menu::where('menu_in','<>','main')
                        ->where('record_status','N')        
                        ->orderby('menu_order')
                        ->get(); 
        
        $data['parent'] = inv_parent::where('system_delete','none')
                        ->orderby('id')
                        ->paginate(20);
                        //->get();

        return view('parent.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['menu'] = sys_menu::where('menu_in','main')
                        ->where('record_status','N')        
                        ->orderby('menu_order')
                        ->get();
        $data['sub_menu'] = sys_menu::where('menu_in','<>','main')
                        ->where('record_status','N')        
                        ->orderby('menu_order')
                        ->get();         
                                
        return view('parent.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = inv_parent::max('id');
        $id = $id+1;

        $parent = new inv_parent();
        $parent->id               = $id;
        $parent->PARENT_NAME_TH   = $request->PARENT_NAME_TH;
        $parent->PARENT_NAME_EN   = $request->PARENT_NAME_EN;
        $parent->PARENT_DETAIL    = $request->PARENT_DETAIL;
        $parent->RECORD_STATUS    = $request->RECORD_STATUS;
        $parent->system_delete    = 'none';

        #return $parent;
        $parent->save();
        return redirect()->action('ParentController@index')->with('alert','บันทึกข้อมูลเรียบร้อยแล้ว');  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['menu'] = sys_menu::where('menu_in','main')
                        ->where('record_status','N')        
                        ->orderby('menu_order')
                        ->get();
        $data['sub_menu'] = sys_menu::where('menu_in','<>','main')
                        ->where('record_status','N')        
                        ->orderby('menu_order')
                        ->get(); 
        
        $data['parent'] = inv_parent::where('id',$id)
                        ->get();

        return view('parent.edit',$data);    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $parent = inv_parent::find($id);
        $parent->PARENT_NAME_TH   = $request->PARENT_NAME_TH;
        $parent->PARENT_NAME_EN   = $request->PARENT_NAME_EN;
        $parent->PARENT_DETAIL    = $request->PARENT_DETAIL;
        $parent->RECORD_STATUS    = $request->RECORD_STATUS;    

        #return $parent;
        $parent->save();
        return redirect()->action('ParentController@index')->with('alert','บันทึกข้อมูลเรียบร้อยแล้ว'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $update = inv_parent::find($id);
        $update->system_delete = "delete";        
        $update->save();  
        return back()->with('alert-class','success')->with('alert','ลบข้อมูลแล้ว');
        #return "ddddd";
    }

    public function change_status(Request $request)
    {
        // return "change_status : ".$id;
        $update = inv_parent::find($request->id);
        $update->RECORD_STATUS = $request->RECORD_STATUS;
        #$update->updated_by = Auth::user()->username;
        $update->save();
        if($request->RECORD_STATUS == 'N'){
            return back()->with('alert-class','success')->with('alert','ออนไลน์เนื้อหาแล้ว');
        }else{
            return back()->with('alert-class','danger')->with('alert','ออฟไลน์เนื้อหาแล้ว');
        }
    }
}
