<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\slip_item;
use App\sys_menu;
use App\sys_month;
use App\sys_year;
use App\mas_campus;
use App\vw_slip_income;
use Auth;
use App\vw_his_person;


class IncomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
        
        $data['income'] = slip_item::where('system_delete','none')
                        ->where('item_type','income')
                        ->orderby('id')
                        ->paginate(20);
                        //->get();

        return view('income.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['menu'] = sys_menu::where('menu_in','main')
                        ->where('record_status','N')       
                        ->where('system_delete','none')    
                        ->orderby('menu_order')
                        ->get();
        $data['sub_menu'] = sys_menu::where('menu_in','<>','main')
                        ->where('record_status','N')       
                        ->where('system_delete','none')    
                        ->orderby('menu_order')
                        ->get();            
        $data['campus']  = mas_campus::where('record_status','N') 
                        ->orderby('campus_id')
                        ->get();

        return view('income.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = slip_item::max('id');
        $id = $id+1;

        $arrCampus = json_encode($request->item_campus);
        #return $arrCampus;

        $income = new slip_item();
        $income->id                 = $id;
        $income->item_name_th       = $request->item_name_th;
        $income->item_name_en       = $request->item_name_en;
        $income->item_campus        = $arrCampus;
        $income->item_price         = $request->item_price;
        $income->item_rate          = $request->item_rate;
        $income->item_code          = $request->item_code;
        $income->RECORD_STATUS      = $request->RECORD_STATUS;
        $income->item_type          = 'income';
        $income->system_delete      = 'none';

        #return $expend;
        $income->save();
        return redirect()->action('IncomeController@index')->with('alert','บันทึกข้อมูลเรียบร้อยแล้ว'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id=null)
    {
        if (empty($id)) {
            #$select_M = date('n');
            $M = vw_slip_income::select('slip_month')
                        ->where('person_id', Auth::user()->vw_his_person->person_id)
                        ->orderby('slip_month','DESC')
                        ->first();   
            $select_M = $M->slip_month;  
        }else{
            $select_M = $id;            
        }    
        #return $select_M;
        $select_Y = date('Y');
        $data['select_M'] = $select_M;
        $data['select_Y'] = $select_Y;  
        
        /*
        $data['menu'] = sys_menu::where('menu_in','main')
                        ->where('record_status','N')     
                        ->where('system_delete','none')     
                        ->orderby('menu_order')
                        ->get();
        $data['sub_menu'] = sys_menu::where('menu_in','<>','main')
                        ->where('record_status','N')   
                        ->where('system_delete','none')       
                        ->orderby('menu_order')
                        ->get();
        */
                        
        $data['sys_month'] = sys_month::where('record_status','N')->orderby('id')->get();
        $data['sys_year'] = sys_year::where('record_status','N')->orderby('id','desc')->get();
        $data['income'] = vw_slip_income::where('person_id', Auth::user()->vw_his_person->person_id)
                            ->where('slip_month',$select_M)
                            ->get();
        $data['his_person'] = vw_his_person::where('person_id',Auth::user()->vw_his_person->person_id)->first();

        #return $data;
        return view('income.show',$data);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {               
        $data['campus']  = mas_campus::where('record_status','N') 
                        ->orderby('campus_id')
                        ->get();
        $data['income'] = slip_item::where('id',$id)
                        ->get();

        return view('income.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $arrCampus = json_encode($request->item_campus);

        $income = slip_item::find($id);    
        $income->item_name_th        = $request->item_name_th;
        $income->item_name_en        = $request->item_name_en;
        $income->item_campus         = $arrCampus;
        $income->item_price          = $request->item_price;
        $income->item_rate           = $request->item_rate;
        $income->item_code          = $request->item_code;
        $income->RECORD_STATUS      = $request->RECORD_STATUS;

        #return $expend;
        $income->save();        
        return redirect()->action('IncomeController@index')->with('alert','บันทึกข้อมูลเรียบร้อยแล้ว');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $update = slip_item::find($id);
        $update->system_delete = "delete";        
        $update->save();  
        return back()->with('alert-class','success')->with('alert','ลบข้อมูลแล้ว');
        #return "ddddd";
    }

    public function change_status(Request $request)
    {
        // return "change_status : ".$id;
        $update = slip_item::find($request->id);
        $update->RECORD_STATUS = $request->RECORD_STATUS;
        #$update->updated_by = Auth::user()->username;
        $update->save();
        if($request->RECORD_STATUS == 'N'){
            return back()->with('alert-class','success')->with('alert','ออนไลน์เนื้อหาแล้ว');
        }else{
            return back()->with('alert-class','danger')->with('alert','ออฟไลน์เนื้อหาแล้ว');
        }
    }
}
