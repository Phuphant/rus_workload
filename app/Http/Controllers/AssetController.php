<?php

namespace App\Http\Controllers;

use App\sys_menu;
use App\inv_asset;
use App\vw_asset;
use App\mas_faculty;
use App\inv_method;
use App\inv_unit;
use App\inv_parent;
use App\inv_chart;

use Illuminate\Http\Request;

class AssetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['menu'] = sys_menu::where('menu_in','main')
                        ->where('record_status','N')        
                        ->orderby('menu_order')
                        ->get();
        $data['sub_menu'] = sys_menu::where('menu_in','<>','main')
                        ->where('record_status','N')        
                        ->orderby('menu_order')
                        ->get(); 
        
        $data['asset'] = vw_asset::where('system_delete','none')
                        ->orderby('id')                        
                        ->paginate(20);

        return view('asset.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['menu'] = sys_menu::where('menu_in','main')
                        ->where('record_status','N')        
                        ->orderby('menu_order')
                        ->get();
        $data['sub_menu'] = sys_menu::where('menu_in','<>','main')
                        ->where('record_status','N')        
                        ->orderby('menu_order')
                        ->get(); 
            
        $data['unit'] = inv_unit::where('RECORD_STATUS','N')
                        ->orderby('id')
                        ->get();
        
        $data['method'] = inv_method::where('RECORD_STATUS','N')
                        ->orderby('id')
                        ->get();
        
        $data['parent'] = inv_parent::where('RECORD_STATUS','N')
                        ->orderby('id')
                        ->get();

        $data['faculty'] = mas_faculty::where('record_status','N')      
                        ->orderby('faculty_id')            
                        ->get();
        
        $data['chart'] = inv_chart::where('RECORD_STATUS','N')      
                        ->orderby('id')            
                        ->get();                        
                                
        return view('asset.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = inv_asset::max('id');
        $id = $id+1;

        $asset = new inv_asset();
        $asset->id              = $id;
        $asset->PARENT_ID       = $request->PARENT_ID;
        $asset->ASSET_NAME_TH   = $request->ASSET_NAME_TH;
        $asset->ASSET_NAME_EN   = $request->ASSET_NAME_EN;
        $asset->ASSET_DESC      = $request->ASSET_DESC;
        $asset->QUANTITY        = $request->QUANTITY;
        $asset->UNIT_ID         = $request->UNIT_ID;
        $asset->UNIT_PRICE      = $request->UNIT_PRICE;
        $asset->METHOD_ID       = $request->METHOD_ID;
        $asset->ORG_ID          = $request->ORG_ID;
        $asset->system_delete   = 'none';
        $asset->RECORD_STATUS   = $request->RECORD_STATUS;

        $asset->save();
        #return $request;
        return redirect()->action('AssetController@index')->with('alert','บันทึกข้อมูลเรียบร้อยแล้ว');        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $update = inv_asset::find($id);
        $update->system_delete = "delete";        
        $update->save();  
        return back()->with('alert-class','success')->with('alert','ลบข้อมูลแล้ว');
        #return "ddddd";
    }

    public function change_status(Request $request)
    {
        // return "change_status : ".$id;
        $update = inv_asset::find($request->id);
        $update->RECORD_STATUS = $request->RECORD_STATUS;
        #$update->updated_by = Auth::user()->username;
        $update->save();
        if($request->RECORD_STATUS == 'N'){
            return back()->with('alert-class','success')->with('alert','ออนไลน์เนื้อหาแล้ว');
        }else{
            return back()->with('alert-class','danger')->with('alert','ออฟไลน์เนื้อหาแล้ว');
        }
    }

}
