<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\slip_item;
use App\sys_month;
use App\sys_year;
use App\mas_campus;
use App\vw_slip_income;
use Auth;
use App\vw_his_person;
use App\vw_inc_net;
use PDF;

class ExportController extends Controller
{
   public function pdf($val)
   {                    
         session()->put('val',$val);
         $data['sys_month'] = sys_month::where('record_status','N')->where('id',$val)->first();
         $data['sys_year'] = date('Y')+543;
         $data['slip_item'] = slip_item::where('RECORD_STATUS','N')->get();
         $his_person = vw_his_person::where('id_card',Auth::user()->vw_his_person->id_card)->first();
         

         $data['list_inc']   = slip_item::where('RECORD_STATUS','N')->where('item_type','income')->get();
         $data['list_exp']   = slip_item::where('RECORD_STATUS','N')->where('item_type','expend')->get();
                  
         $data['inc_net'] = vw_inc_net::where('person_id', $his_person->person_id)                              
                              ->where('slip_month',$val)                          
                              ->where('slip_year','2022')  
                              ->first();  
         
         $data['cur_date'] = date('d-m-Y');
         $data['cur_time'] = date('H:i:s');
         
         #return $his_person->person_id;

         #return view('summary.show',$data);
         return view('exportPDF.export',$data);
   }

   public function createPDF()
   {
     # return session('val');
   /* $data['sys_month'] = sys_month::where('record_status','N')->orderby('id')->get();
    $data['sys_year'] = sys_year::where('record_status','N')->orderby('id','desc')->get();
    $data['slip_item'] = slip_item::where('RECORD_STATUS','N')->get();
    $data['inc_net'] = vw_inc_net::where('person_id', Auth::user()->vw_his_person->person_id)
                        ->where('slip_month',session('val'))                            
                        ->get();        
    $data['his_person'] = vw_his_person::where('person_id',Auth::user()->vw_his_person->person_id)->first(); */
       // retreive all records from db
      #$data = sys_menu::all();
      #return $data;
      // share data to view
      #view()->share('employee',$data);
      $data['sys_month'] = sys_month::where('record_status','N')->where('id',$val)->first();
      $data['sys_year'] = date('Y')+543;
      
      $data['slip_item'] = slip_item::where('RECORD_STATUS','N')->get();
      $his_person = vw_his_person::where('id_card',Auth::user()->vw_his_person->id_card)->first();
      

      $data['list_inc']   = slip_item::where('RECORD_STATUS','N')->where('item_type','income')->get();
      $data['list_exp']   = slip_item::where('RECORD_STATUS','N')->where('item_type','expend')->get();
               
      $data['inc_net'] = vw_inc_net::where('person_id', $his_person->person_id)                              
                           ->where('slip_month',session('val'))                          
                           ->where('slip_year','2022')  
                           ->first();  

      $pdf = PDF::loadView('exportPDF.export',$data);      
      // download PDF file with download method
      return $pdf->download('export.pdf');
   }
}
