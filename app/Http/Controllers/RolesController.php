<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\sys_menu;
use App\roles;
use Auth;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['menu'] = sys_menu::where('menu_in','main')
                        ->where('record_status','N')     
                        ->where('system_delete','none')     
                        ->orderby('menu_order')
                        ->get();
        $data['sub_menu'] = sys_menu::where('menu_in','<>','main')
                        ->where('record_status','N')   
                        ->where('system_delete','none')       
                        ->orderby('menu_order')
                        ->get();
                        
        $data['roles'] = roles::all();

        return view('role.group.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['menu'] = sys_menu::where('menu_in','main')
                        ->where('record_status','N')     
                        ->where('system_delete','none')     
                        ->orderby('menu_order')
                        ->get();
        $data['sub_menu'] = sys_menu::where('menu_in','<>','main')
                        ->where('record_status','N')   
                        ->where('system_delete','none')       
                        ->orderby('menu_order')
                        ->get();        

        return view('role.group.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = roles::max('id');
        $id = $id+1;

        $user = Auth::user()->username;

        $arrMenu = json_encode($request->item_menu);

        $roles = new roles;
        $roles->id          =   $id;
        $roles->code        =   $request->code;
        $roles->name        =   $request->name;
        $roles->can_access  =   $arrMenu;
        $roles->level       =   $request->level;
        $roles->record_status = $request->RECORD_STATUS;
        $roles->created_by   =  Auth::user()->username;
        
        $roles->save();
        #return $roles;
        return redirect()->action('RolesController@index')->with('alert','บันทึกข้อมูลเรียบร้อยแล้ว'); 

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
