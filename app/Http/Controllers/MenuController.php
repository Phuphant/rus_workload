<?php

namespace App\Http\Controllers;
use App\sys_menu;
use Auth;

use Illuminate\Http\Request;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {         
                        
        $data['MenuContent'] = sys_menu::where('menu_in','main')    
                        ->where('system_delete','none')    
                        ->orderby('menu_order')
                        ->get();
        $data['SubMenuContent'] = sys_menu::where('menu_in','<>','main')    
                        ->where('system_delete','none')                        
                        ->orderby('menu_order')
                        ->get();                        
/*
        $data['topic_name'] = tb_topic_poll::where('topic_group',$system)
                                ->where('topic_id_sub','<>','root')
                                ->where('topic_status','on')
                                ->get();
*/
        #return view('demo_layout.index');
        #return view('layout.master');
        //dd($data);
        return view('menu.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['MenuContent'] = sys_menu::where('menu_in','main')    
                                ->where('system_delete','none')    
                                ->orderby('menu_order')
                                ->get();
        return view('menu.create',$data);        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = sys_menu::max('id');
        $id = $id+1;

        $menu = new sys_menu;
        $menu->id           = $id;
        $menu->menu_order   = $request->menu_order;
        $menu->menu_in      = $request->menu_in;
        $menu->menu_name_th = $request->menu_name_th;
        $menu->menu_target  = $request->menu_target;
        $menu->menu_path    = $request->menu_path;
        $menu->record_status = $request->RECORD_STATUS;
        $menu->menu_code    = $request->menu_code;
        $menu->system_delete = 'none';
        $menu->created_user = Auth::user()->username;
        $menu->save();
        
        #return $menu;
        return redirect('manage/menu')->with('alert','บันทึกข้อมูลเรียบร้อยแล้ว');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $data['Menu'] = sys_menu::find($id);
        $data['MenuContent'] = sys_menu::where('menu_in','main')    
                                ->where('system_delete','none')    
                                ->orderby('menu_order')
                                ->get();

        return view('menu.edit',$data);        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $menu = sys_menu::find($id);        
        $menu->menu_order   = $request->menu_order;
        $menu->menu_in      = $request->menu_in;
        $menu->menu_name_th = $request->menu_name_th;
        $menu->menu_target  = $request->menu_target;
        $menu->menu_path    = $request->menu_path;
        $menu->record_status = $request->RECORD_STATUS;
        $menu->menu_code    = $request->menu_code;
        $menu->system_delete = 'none';
        $menu->updated_user = Auth::user()->username;
        $menu->save();
        
        #return $menu;
        return redirect('manage/menu')->with('alert','บันทึกข้อมูลเรียบร้อยแล้ว');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $update = sys_menu::find($id);
        $update->system_delete = "delete";        
        $update->save();  
        return back()->with('alert-class','success')->with('alert','ลบข้อมูลแล้ว');
        #return "ddddd";
    }

    public function change_status(Request $request)
    {
        // return "change_status : ".$id;
        $update = sys_menu::find($request->id);        
        $update->record_status = $request->RECORD_STATUS;
        #$update->updated_by = Auth::user()->username;
        $update->save();
        if($request->record_status == 'N'){
            return back()->with('alert-class','success')->with('alert','ออนไลน์เนื้อหาแล้ว');
        }else{
            return back()->with('alert-class','danger')->with('alert','ออฟไลน์เนื้อหาแล้ว');
        }
    }
}
