<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\slip_item;
use App\sys_menu;
use App\sys_month;
use App\sys_year;
use App\mas_campus;
use App\vw_slip_income;
use Auth;
use App\vw_his_person;
use App\vw_inc_net;

class SummaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id=null)
    {
        if (empty($id)) {
            $M = vw_inc_net::select('slip_month')
                        ->where('person_id', Auth::user()->vw_his_person->person_id)
                        ->orderby('slip_month','DESC')
                        ->first();   
            $select_M = $M->slip_month;             
        }else{
            $select_M = $id;            
        } 
        #return $select_M;
        $select_Y = date('Y');
        $data['select_M'] = $select_M;
        $data['select_Y'] = $select_Y;
                

        $data['sys_month'] = sys_month::where('record_status','N')->orderby('id')->get();
        $data['sys_year'] = sys_year::where('record_status','N')->orderby('id','desc')->get();
        $data['slip_item'] = slip_item::where('RECORD_STATUS','N')->get();
        $data['inc_net'] = vw_inc_net::where('person_id', Auth::user()->vw_his_person->person_id)
                            ->where('slip_month',$select_M)                            
                            ->get();        
        $data['his_person'] = vw_his_person::where('person_id',Auth::user()->vw_his_person->person_id)->first();

        return view('summary.show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
