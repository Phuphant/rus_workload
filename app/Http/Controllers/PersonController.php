<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\sys_menu;
use App\vw_his_person;
use App\User;
use App\mas_faculty;
use App\permissions;
use Auth;

class PersonController extends Controller
{

    public function index()
    {       
        $count = permissions::where('person_id',Auth::user()->id_card)->where('role_id',0)->where('record_status','N')->count();
        #return $count;
        if ($count == 0) {
            $user = vw_his_person::where('id_card',Auth::user()->id_card)->first();            
            $id = permissions::select('id')->orderby('id','DESC')->first();
            $id = $id->id+1;
            
            $permis = new permissions;
            $permis->id = $id;
            $permis->person_id = Auth::user()->id_card;
            $permis->role_id = 0;
            $permis->fac_id = $user->faculty_id;
            $permis->camp_id = $user->campus_id;
            $permis->record_status = 'N';      
            $permis->created_by = Auth::user()->username;      
            
            #return $permis;
            $permis->save();
            session()->put('idcard',Auth::user()->id_card);
        }else{
            $permis = permissions::where('person_id',Auth::user()->id_card)->where('role_id',0)->where('record_status','N')->first();
        }
        
        return redirect('manage/changepermis/'.$permis->role_id);
        #return  $data['his_person'];                           
        #return view('person.index',$data);
    }

    public function show_detail()
    {
        #return Auth::user()->id_card."-".Auth::user()->username;
        #$data['user'] = User::where('id_card',Auth::user()->id_card)->get();
        #$data['his_person'] = vw_his_person::where('id_card',Auth::user()->id_card)->get();
        $data['cur_date'] = date('d-m-Y H:i:s');
        #return $data;
        
        return view('person.index',$data);
    }
}
