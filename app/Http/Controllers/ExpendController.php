<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\slip_item;
use App\sys_menu;
use App\sys_month;
use App\sys_year;
use App\mas_campus;
use App\vw_slip_expend;
use Auth;
use App\vw_his_person;

class ExpendController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $camp_id = session('camp_id');
        #return $camp_id;
       /*
        $data['menu'] = sys_menu::where('menu_in','main')
                        ->where('record_status','N')   
                        ->where('system_delete','none')        
                        ->orderby('menu_order')
                        ->get();
        $data['sub_menu'] = sys_menu::where('menu_in','<>','main')
                        ->where('record_status','N')  
                        ->where('system_delete','none')         
                        ->orderby('menu_order')
                        ->get(); 
       */                
        $data['expend'] = slip_item::where('RECORD_STATUS','N')
                        ->where('item_type','expend')       
                        #->whereIn('item_campus',['1'])                 
                        ->where('item_campus','LIKE','%'.$camp_id.'%')
                        ->orderby('id')
                        ->paginate(20);
                        #->get();                                                  
        #return $data;
        return view('expend.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        /*
        $data['menu'] = sys_menu::where('menu_in','main')
                        ->where('record_status','N')       
                        ->where('system_delete','none')    
                        ->orderby('menu_order')
                        ->get();
        $data['sub_menu'] = sys_menu::where('menu_in','<>','main')
                        ->where('record_status','N')       
                        ->where('system_delete','none')    
                        ->orderby('menu_order')
                        ->get();  
        */          
        $data['campus']  = mas_campus::where('record_status','N') 
                        ->orderby('campus_id')
                        ->get();

        
        return view('expend.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = slip_item::max('id');
        $id = $id+1;

        $arrCampus = json_encode($request->item_campus);
        #return $arrCampus;

        $expend = new slip_item();
        $expend->id                 = $id;
        $expend->item_name_th        = $request->item_name_th;
        $expend->item_name_en        = $request->item_name_en;
        $expend->item_campus         = $arrCampus;
        $expend->item_price          = $request->item_price;
        $expend->item_rate           = $request->item_rate;
        $expend->RECORD_STATUS      = $request->RECORD_STATUS;
        $expend->item_type          = 'expend';
        $expend->system_delete      = 'none';

        #return $expend;
        $expend->save();
        return redirect()->action('ExpendController@index')->with('alert','บันทึกข้อมูลเรียบร้อยแล้ว'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id=null)
    {
        $camp_id = session('camp_id');
        #return session('camp_id');
        if (empty($id)) {
            #$select_M = date('n');  
            $M = vw_slip_expend::select('slip_month')
                        ->where('person_id', Auth::user()->vw_his_person->person_id)
                        ->orderby('slip_month','DESC')
                        ->first();   
            $select_M = $M->slip_month;           
        }else{
            $select_M = $id;                    
        }    
        #return $select_M;
        $select_Y = date('Y');
        $data['select_M'] = $select_M;
        $data['select_Y'] = $select_Y;  
             
        $data['sys_month'] = sys_month::where('record_status','N')->orderby('id')->get();
        $data['sys_year'] = sys_year::where('record_status','N')->orderby('id','desc')->get();
        $data['slip_item'] = slip_item::where('RECORD_STATUS','N')->where('item_campus','LIKE','%'.$camp_id.'%')->get();
        #return $data['slip_item'];
        $data['expend'] = vw_slip_expend::where('person_id', Auth::user()->vw_his_person->person_id)
                            ->where('slip_month',$select_M)
                            ->get();
        $data['his_person'] = vw_his_person::where('person_id',Auth::user()->vw_his_person->person_id)->first();

        return view('expend.show',$data);     
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        /*
         $data['menu'] = sys_menu::where('menu_in','main')
                        ->where('record_status','N')       
                        ->where('system_delete','none')    
                        ->orderby('menu_order')
                        ->get();
        $data['sub_menu'] = sys_menu::where('menu_in','<>','main')
                        ->where('record_status','N')       
                        ->where('system_delete','none')    
                        ->orderby('menu_order')
                        ->get();
         */            
        $data['campus']  = mas_campus::where('record_status','N') 
                        ->orderby('campus_id')
                        ->get();
        $data['expend'] = slip_item::where('id',$id)
                        ->get();

        return view('expend.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $arrCampus = json_encode($request->item_campus);

        $expend = slip_item::find($id);    
        $expend->item_name_th        = $request->item_name_th;
        $expend->item_name_en        = $request->item_name_en;
        $expend->item_campus         = $arrCampus;
        $expend->item_price          = $request->item_price;
        $expend->item_rate           = $request->item_rate;
        $expend->RECORD_STATUS      = $request->RECORD_STATUS;

        #return $expend;
        $expend->save();        
        return redirect()->action('ExpendController@index')->with('alert','บันทึกข้อมูลเรียบร้อยแล้ว');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $update = slip_item::find($id);
        $update->system_delete = "delete";        
        $update->save();  
        return back()->with('alert-class','success')->with('alert','ลบข้อมูลแล้ว');
        #return "ddddd";
    }

    public function change_status(Request $request)
    {
        // return "change_status : ".$id;
        $update = slip_item::find($request->id);
        $update->RECORD_STATUS = $request->RECORD_STATUS;
        #$update->updated_by = Auth::user()->username;
        $update->save();
        if($request->RECORD_STATUS == 'N'){
            return back()->with('alert-class','success')->with('alert','ออนไลน์เนื้อหาแล้ว');
        }else{
            return back()->with('alert-class','danger')->with('alert','ออฟไลน์เนื้อหาแล้ว');
        }
    }
}