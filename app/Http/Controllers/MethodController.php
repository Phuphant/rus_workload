<?php

namespace App\Http\Controllers;

use App\sys_menu;
use App\inv_method;

use Illuminate\Http\Request;

class MethodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['menu'] = sys_menu::where('menu_in','main')
                        ->where('record_status','N')        
                        ->orderby('menu_order')
                        ->get();
        $data['sub_menu'] = sys_menu::where('menu_in','<>','main')
                        ->where('record_status','N')        
                        ->orderby('menu_order')
                        ->get(); 
        
        $data['method'] = inv_method::where('system_delete','none')
                        ->orderby('id')
                        ->paginate(20);
                        //->get();

        return view('method.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['menu'] = sys_menu::where('menu_in','main')
                        ->where('record_status','N')        
                        ->orderby('menu_order')
                        ->get();
        $data['sub_menu'] = sys_menu::where('menu_in','<>','main')
                        ->where('record_status','N')        
                        ->orderby('menu_order')
                        ->get();             

        return view('method.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = inv_method::max('id');
        $id = $id+1;

        $method = new inv_method();
        $method->id               = $id;
        $method->METHOD_CODE      = $request->METHOD_CODE;
        $method->METHOD_NAME_TH   = $request->METHOD_NAME_TH;
        $method->METHOD_NAME_EN   = $request->METHOD_NAME_EN;
        $method->METHOD_DETAIL    = $request->METHOD_DETAIL;
        $method->RECORD_STATUS    = $request->RECORD_STATUS;
        $method->system_delete    = 'none';

        #return $unit;
        $method->save();
        return redirect()->action('MethodController@index')->with('alert','บันทึกข้อมูลเรียบร้อยแล้ว'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['menu'] = sys_menu::where('menu_in','main')
                        ->where('record_status','N')        
                        ->orderby('menu_order')
                        ->get();
        $data['sub_menu'] = sys_menu::where('menu_in','<>','main')
                        ->where('record_status','N')        
                        ->orderby('menu_order')
                        ->get(); 
        
        $data['method'] = inv_method::where('id',$id)
                        ->get();

        return view('method.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        #$unit = inv_unit::where('UNIT_ID',$id);
        $method = inv_method::find($id);
        $method->METHOD_CODE      = $request->METHOD_CODE;
        $method->METHOD_NAME_TH     = $request->METHOD_NAME_TH;
        $method->METHOD_NAME_EN     = $request->METHOD_NAME_EN;
        $method->METHOD_DETAIL     = $request->METHOD_DETAIL;
        $method->RECORD_STATUS    = $request->RECORD_STATUS;
        $method->save();
        #return $request;
        return redirect()->action('MethodController@index')->with('alert','บันทึกข้อมูลเรียบร้อยแล้ว');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $update = inv_method::find($id);
        $update->system_delete = "delete";        
        $update->save();  
        return back()->with('alert-class','success')->with('alert','ลบข้อมูลแล้ว');
        #return "ddddd";
    }

    public function change_status(Request $request)
    {
        // return "change_status : ".$id;
        $update = inv_method::find($request->id);
        $update->RECORD_STATUS = $request->RECORD_STATUS;
        #$update->updated_by = Auth::user()->username;
        $update->save();
        if($request->RECORD_STATUS == 'N'){
            return back()->with('alert-class','success')->with('alert','ออนไลน์เนื้อหาแล้ว');
        }else{
            return back()->with('alert-class','danger')->with('alert','ออฟไลน์เนื้อหาแล้ว');
        }
    }
}
