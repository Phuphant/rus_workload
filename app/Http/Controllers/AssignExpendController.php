<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\sys_menu;
use App\mas_campus;
use App\slip_item;
use App\vw_his_person;
use App\mas_faculty;
use App\mas_workline;
use App\slip_match_per_expend;
use App\slip_per_price_expend;

class AssignExpendController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['menu'] = sys_menu::where('menu_in','main')
                        ->where('record_status','N')   
                        ->where('system_delete','none')        
                        ->orderby('menu_order')
                        ->get();
        $data['sub_menu'] = sys_menu::where('menu_in','<>','main')
                        ->where('record_status','N')  
                        ->where('system_delete','none')         
                        ->orderby('menu_order')
                        ->get(); 
        
        $data['expend'] = slip_item::where('system_delete','none')
                        ->where('item_type','expend')
                        ->where('RECORD_STATUS','N')
                        ->orderby('id')
                        ->get();

        return view('expend.assign',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return "222222";
    }

    public function create_person(Request $request, $id)
    {
        $data['menu'] = sys_menu::where('menu_in','main')
                        ->where('record_status','N')   
                        ->where('system_delete','none')        
                        ->orderby('menu_order')
                        ->get();
        $data['sub_menu'] = sys_menu::where('menu_in','<>','main')
                        ->where('record_status','N')  
                        ->where('system_delete','none')         
                        ->orderby('menu_order')
                        ->get();
        $data['expend'] = slip_item::where('id',$id)->get();       
        $data['faculty'] = mas_faculty::where('record_status','N')->orderby('faculty_id')->get();
        $data['campus'] = mas_campus::where('record_status','N')->orderby('campus_id')->get();
        $data['workline'] = mas_workline::where('record_status','N')->orderby('workline_id')->get();
                           
        $data['search'] = slip_match_per_expend::with('vw_his_person')->where('system_delete','none')->where('record_status','N')->where('exp_id','like','%'.$id.'%');
        $data['search'] = $data['search']->get();
                        /*->paginate(20);*/
        
        $data['facu_id'] = $request->faculty;
        $data['camp_id'] = $request->campus;
        $data['work_id'] = $request->workline;   
        $data['exp_month'] = date("m");
        $data['exp_year'] = date("Y");
        
                     
        #$data['per_faculty'] = mas_faculty::with('vw_his_person')->whereIn('faculty_id',['9'])->get();
        /*
        $data['his_person'] = vw_his_person::where('record_status','N')
                        ->where('statuslist','9')
                        ->get();
         */
        
        return view('expend.frm_ass_person',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        #$Person = json_encode($request->chk_person);
        #$Expprice = json_encode($request->exp_price);

        // return $Person."-".$Expprice;
        // return count($request->chk_person);
        #return $request->chk_person[0];
        #return strpos($request->exp_price,$request->chk_person[0]);
        // return $request->data[2];
        #return $request->all();
        for($i = 0 ; $i < count($request->chk_person) ; $i++){
            // $a = $request->chk_person[$i];
             $person_id = $request->data[$request->chk_person[$i]]['person_id'];
             $exp_price = $request->data[$request->chk_person[$i]]['exp_price'];
            #print $person_id."=>".$exp_price."<br>";

            $per_expend = new slip_per_price_expend();
            $per_expend->person_id          = $person_id;
            $per_expend->exp_id             = $request->exp_id;
            $per_expend->exp_price          = $exp_price;
            $per_expend->campus_id          = $request->campus_id;   
            $per_expend->exp_month          = $request->exp_month;
            $per_expend->exp_year           = $request->exp_year;
            $per_expend->record_status      = 'N';
            $per_expend->system_delete      = 'none';

            #return $per_expend;
            $per_expend->save();
        }
        // exit();

        $data['menu'] = sys_menu::where('menu_in','main')
                        ->where('record_status','N')   
                        ->where('system_delete','none')        
                        ->orderby('menu_order')
                        ->get();
        $data['sub_menu'] = sys_menu::where('menu_in','<>','main')
                        ->where('record_status','N')  
                        ->where('system_delete','none')         
                        ->orderby('menu_order')
                        ->get();
        $data['expend'] = slip_item::where('id',$request->exp_id)->get();    
        #$data['assign_price'] = slip_per_price_expend::where('id',$id)->get();       
        $data['vw_his_person'] = vw_his_person::where('statuslist','9')->get();    

        return view('expend.assign_expend',$data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function match_person()
    {        
        $data['menu'] = sys_menu::where('menu_in','main')
                        ->where('record_status','N')   
                        ->where('system_delete','none')        
                        ->orderby('menu_order')
                        ->get();
        $data['sub_menu'] = sys_menu::where('menu_in','<>','main')
                        ->where('record_status','N')  
                        ->where('system_delete','none')         
                        ->orderby('menu_order')
                        ->get();        
        $data['campus'] = mas_campus::where('record_status','N')
                        ->orderby('campus_id')
                        ->get();                  

        $data['search'] = vw_his_person::where('statuslist','9');
            if (isset($request->faculty)) {
                $data['search'] = $data['search']->where('faculty_id',$request->faculty);
            }

            if (isset($request->campus)) {
                $data['search'] = $data['search']->where('campus_id',$request->campus);
            }

            if (isset($request->workline)) {
                $data['search'] = $data['search']->where('workline_id',$request->workline);
            }

            if (isset($request->name_search)) {
                $data['search'] = $data['search']->where('fname_th',$request->name_search)->orwhere('lname_th',$request->name_search);
            }        
        $data['search'] = $data['search']->get();

        $data['per_faculty'] = mas_faculty::with('vw_his_person.slip_match_per_expend')->whereIn('faculty_id',['9'])->get();
        #$data['per_faculty'] = mas_faculty::with('vw_his_person')->whereIn('faculty_id',['9'])->get();
        $data['per_expend'] = slip_match_per_expend::where('system_delete','none')->where('record_status','N')->get();
        
        return view('expend.frm_match_person',$data);
    }

    public function match_person_step2($id)
    {
        $data['menu'] = sys_menu::where('menu_in','main')
                        ->where('record_status','N')   
                        ->where('system_delete','none')        
                        ->orderby('menu_order')
                        ->get();
        $data['sub_menu'] = sys_menu::where('menu_in','<>','main')
                        ->where('record_status','N')  
                        ->where('system_delete','none')         
                        ->orderby('menu_order')
                        ->get();       
        $data['expend'] = slip_item::where('system_delete','none')
                            ->where('RECORD_STATUS','N')
                            ->where('item_type','expend')
                            ->where('item_campus','like','%1%')
                            ->orderby('id')->get();
        $data['person'] = vw_his_person::where('person_id',$id)->get();
        $data['id'] = $id;
                
        return view('expend.frm_match_person_step2',$data);            
    }

    public function create_person_expend(Request $request)
    {        
        $exp_id = json_encode($request->chk_expend);

        $chk_per = slip_match_per_expend::where('person_id',$request->person_id)->value('id');  
        #return $chk_per;
        
        if (isset($chk_per)) {
            // UPDATE status = D
            $match_per_exp = slip_match_per_expend::find($chk_per);  
            $match_per_exp->record_status = 'D';
            $match_per_exp->save();

            // INSERT new record
            $match_per_exp = new slip_match_per_expend();
            $match_per_exp->person_id       = $request->person_id;
            $match_per_exp->exp_id          = $exp_id;
            $match_per_exp->system_delete   = 'none';
            $match_per_exp->record_status   = 'N';            
            $match_per_exp->save();

            return redirect()->action('AssignExpendController@match_person')->with('alert','บันทึกข้อมูลเรียบร้อยแล้ว');

        }else{            
            $match_per_exp = new slip_match_per_expend();
            $match_per_exp->person_id       = $request->person_id;
            $match_per_exp->exp_id          = $exp_id;
            $match_per_exp->system_delete   = 'none';
            $match_per_exp->record_status   = 'N';            
            $match_per_exp->save();

            return redirect()->action('AssignExpendController@match_person')->with('alert','บันทึกข้อมูลเรียบร้อยแล้ว');
        }
       
            

        /*
        
        /**/
    }
}
