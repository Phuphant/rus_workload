<?php

namespace App\Http\Controllers;

use App\sys_menu;
use App\vw_permissions;
use Auth;

use Illuminate\Http\Request;

class MainColtroller extends Controller{

    public function index(){

        $data['menu'] = sys_menu::where('menu_in','main')
                        ->where('record_status','N')    
                        ->where('system_delete','none')    
                        ->orderby('menu_order')
                        ->get();

        $data['sub_menu'] = sys_menu::where('menu_in','<>','main')
                        ->where('record_status','N')    
                        ->where('system_delete','none')    
                        ->orderby('menu_order')
                        ->get();                  

               
        #return view('demo_layout.index');
        #return view('layout.master');
        //dd($data);
        return view('main',$data);
    }

}
