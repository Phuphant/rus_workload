<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\vw_his_person;
use App\sys_menu;
use App\mas_faculty;
use App\mas_campus;
use App\mas_workline;
use App\vw_sly_rate;


class SalaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {              
        $data['faculty'] = mas_faculty::where('record_status','N')->orderby('faculty_id')->get();
        $data['campus'] = mas_campus::where('record_status','N')->orderby('campus_id')->get();
        $data['workline'] = mas_workline::where('record_status','N')->orderby('workline_id')->get();                                                

        return view('salary.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function search(Request $request)
    {      
        $data['faculty'] = mas_faculty::where('record_status','N')->orderby('faculty_id')->get();
        $data['campus'] = mas_campus::where('record_status','N')->orderby('campus_id')->get();
        $data['workline'] = mas_workline::where('record_status','N')->orderby('workline_id')->get(); 
        #if(empty($request)){
        #    $data['search'] = vw_his_person::where('statuslist','9');
        #}else{                                     
            $data['search'] = vw_his_person::where('statuslist','9');

                if (isset($request->faculty)) {
                    $data['search'] = $data['search']->where('faculty_id',$request->faculty);
                }

                if (isset($request->campus)) {
                    $data['search'] = $data['search']->where('campus_id',$request->campus);
                }

                if (isset($request->workline)) {
                    $data['search'] = $data['search']->where('workline_id',$request->workline);
                }

                if (isset($request->name_search)) {
                    $data['search'] = $data['search']->where('fname_th',$request->name_search)->orwhere('lname_th',$request->name_search);
                }
            
            $data['search'] = $data['search']->get();
                            /*->paginate(20);*/
                                                                                            
            $data['facu_id'] = $request->faculty;
            $data['camp_id'] = $request->campus;
            $data['work_id'] = $request->workline;
        #}
        
        return view('salary.index',$data);     
    }

    public function salary_rate()
    {

        $data['chk_faculty'] = "";   
        $data['chk_campus']  = session('camp_id');
        $data['chk_workline']  = "";

        $data['faculty'] = mas_faculty::where('record_status','N')->orderby('faculty_id')->get();
        $data['campus'] = mas_campus::where('record_status','N')->orderby('campus_id')->get();
        $data['workline'] = mas_workline::where('record_status','N')->orderby('workline_id')->get(); 
        #$data['sly_rate'] = vw_sly_rate::where('rate_status_id','2')->count();        
        $data['sly_rate'] = vw_his_person::where('statuslist','9')->count();        
        $data['search'] = vw_his_person::where('statuslist','9');
        $data['count_search'] = 0;
        return view('salary.sly',$data);
    }

    public function rate_search(Request $request)
    {
        $data['chk_faculty'] = "";   
        $data['chk_campus']  = session('camp_id');
        $data['chk_workline']  = "";
        $month = date('m')-1;
        $year = date('Y');
        #return $year;

        #return session('role_id') ;

        if(session('role_id')== "2"){
            $camp_id = session('camp_id');
        }
        else{
            $camp_id = "";
        }
        #return $camp_id;
        
        $data['faculty'] = mas_faculty::where('record_status','N')->orderby('faculty_id')->get();
        $data['campus'] = mas_campus::where('record_status','N')->orderby('campus_id')->get();
        $data['workline'] = mas_workline::where('record_status','N')->orderby('workline_id')->get();                          
        #$data['search'] = vw_sly_rate::where('record_status','N');
        #$data['search'] = vw_his_person::where('record_status','N');
        $data['search'] = vw_his_person::with('inc_rate_salary')
                                        ->where('record_status','N')
                                        #->where('slip_month',$month)
                                        #->where('slip_year',$year)
                                        ;                                        

            if (isset($request->faculty)) {
                $data['search'] = $data['search']->where('faculty_id',$request->faculty);
                $data['chk_faculty'] = $request->faculty;
            }
            if (isset($camp_id)) {
                $data['search'] = $data['search']->where('campus_id',$camp_id);
                $data['chk_campus'] = $camp_id;
            }             
            
            if (isset($request->workline)) {
                $data['search'] = $data['search']->where('workline_id',$request->workline);
                $data['chk_workline'] = $request->workline;
            }
            /* 
            if (isset($request->lname_search)) {
                $data['search'] = $data['search']->where('lname_th','LIKE %',$request->lname_search);
            }  
            */                             
        $data['search'] = $data['search']->where('statuslist','9')->get();
        #return $data['search'];
        $data['count_search'] = $data['search']->count();
        
        #return $data;

        return view('salary.sly',$data);     
               
    }
}
