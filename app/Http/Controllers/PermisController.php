<?php

namespace App\Http\Controllers;
use App\sys_menu;
use App\vw_permissions;
use App\vw_his_person;
use App\User;
use App\roles;
use App\mas_faculty;
use App\mas_division;
use App\permissions;
use Auth;

use Illuminate\Http\Request;

class PermisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {                                          
        #$data['permis'] = vw_permissions::where('role_id','<>',0)->get();
        $data['User'] = User::all();
        $data['roles']  = roles::where('record_status','N')->where('id','<>','0')->get();
        $data['permis'] = User::with('permissions')
                #->where('faculty_id','9')
                #->where('statuslist','9')
                #->where('person_id','1161')
                #->whereIn('role_id',['1','2'])
                ->get();
            #return $data;

        return view('role.permissions.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['faculty'] = mas_faculty::where('record_status','N')->get();
        return view('role.permissions.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = permissions::max('id');
        $id = $id+1;

        $his_person = vw_his_person::where('id_card',$request->person_id)->first();
        #return $his_person;
        $fac_id     = $his_person->faculty_id;
        $div_id     = $his_person->division_id;
        $camp_id    = $his_person->campus_id;

        $permis = new permissions();
        $permis->id         = $id;
        $permis->person_id  = $request->person_id;
        $permis->role_id    = $request->role_id;
        $permis->fac_id     = $fac_id;
        $permis->div_id     = $div_id;
        $permis->camp_id    = $camp_id;
        $permis->record_status = 'N';
        $permis->created_by = Auth::user()->username;
        $permis->save();
        
        #return $permis;        
        return redirect('manage/permis');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function changepermis($id=null)
    {
        #return $id."+".Auth::user()->id_card;
        $account =  vw_permissions::where('person_id',Auth::user()->id_card)->where('record_status','N')->where('role_id',$id)->first();      
        #return $account;
        #return $account->can_access;
        
        if($account){
            session()->put('role_id',$account->role_id);
            session()->put('role_name',$account->name);
            session()->put('can_access',$account->can_access);
            session()->put('level',$account->level);
            session()->put('camp_id',$account->camp_id);
        }     
        
        #$arrMenu = json_decode($account->can_access);
        #return $arrMenu;
        /*
        $main_menu = sys_menu::where('menu_in','main')
                        ->where('record_status','N')     
                        ->where('system_delete','none')  
                        ->whereIn('id',$arrMenu)   
                        ->orderby('menu_order')
                        ->get();
        $sub_menu = sys_menu::where('menu_in','<>','main')
                        ->where('record_status','N')   
                        ->where('system_delete','none')    
                        ->orderby('menu_order')
                        ->get();
        
        session()->put('menu',$main_menu);
        session()->put('sub_menu',$sub_menu);
        */

        #return $permis;
        #return redirect()->route('main');

       
        #$data['his_person'] = vw_his_person::where('id_card',Auth::user()->id_card)->get();
        #return view('person.index',$data);
         
        return redirect()->route('profile');
        
    }
}
