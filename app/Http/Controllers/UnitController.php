<?php

namespace App\Http\Controllers;

use App\sys_menu;
use App\inv_unit;

use Illuminate\Http\Request;

class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['menu'] = sys_menu::where('menu_in','main')
                        ->where('record_status','N')        
                        ->orderby('menu_order')
                        ->get();
        $data['sub_menu'] = sys_menu::where('menu_in','<>','main')
                        ->where('record_status','N')        
                        ->orderby('menu_order')
                        ->get(); 
        
        $data['unit'] = inv_unit::where('system_delete','none')
                        ->orderby('id')
                        ->paginate(20);
                        //->get();

        return view('unit.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['menu'] = sys_menu::where('menu_in','main')
                        ->where('record_status','N')        
                        ->orderby('menu_order')
                        ->get();
        $data['sub_menu'] = sys_menu::where('menu_in','<>','main')
                        ->where('record_status','N')        
                        ->orderby('menu_order')
                        ->get(); 
        
        $data['unit'] = inv_unit::orderby('id')
                        ->paginate(20);
        $data['id'] = inv_unit::max('id');
                                
        return view('unit.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = inv_unit::max('id');
        $id = $id+1;

        $unit = new inv_unit();
        $unit->id               = $id;
        $unit->UNIT_CODE        = $request->UNIT_CODE;
        $unit->UNIT_NAME_TH     = $request->UNIT_NAME_TH;
        $unit->UNIT_NAME_EN     = $request->UNIT_NAME_EN;
        $unit->RECORD_STATUS    = $request->RECORD_STATUS;

        #return $unit;
        $unit->save();
        return redirect()->action('UnitController@index')->with('alert','บันทึกข้อมูลเรียบร้อยแล้ว');        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $unit = new inv_unit();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['menu'] = sys_menu::where('menu_in','main')
                        ->where('record_status','N')        
                        ->orderby('menu_order')
                        ->get();
        $data['sub_menu'] = sys_menu::where('menu_in','<>','main')
                        ->where('record_status','N')        
                        ->orderby('menu_order')
                        ->get(); 
        
        $data['unit'] = inv_unit::where('id',$id)
                        ->get();

        return view('unit.edit',$data);    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        #$unit = inv_unit::where('UNIT_ID',$id);
        $unit = inv_unit::find($id);
        $unit->UNIT_CODE        = $request->UNIT_CODE;
        $unit->UNIT_NAME_TH     = $request->UNIT_NAME_TH;
        $unit->UNIT_NAME_EN     = $request->UNIT_NAME_EN;
        $unit->RECORD_STATUS    = $request->RECORD_STATUS;
        $unit->save();
        #return $request;
        return redirect()->action('UnitController@index')->with('alert','บันทึกข้อมูลเรียบร้อยแล้ว');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $update = inv_unit::find($id);
        $update->system_delete = "delete";        
        $update->save();  
        return back()->with('alert-class','success')->with('alert','ลบข้อมูลแล้ว');
        #return "ddddd";
    }

    public function change_status(Request $request)
    {
        // return "change_status : ".$id;
        $update = inv_unit::find($request->id);
        $update->RECORD_STATUS = $request->RECORD_STATUS;
        #$update->updated_by = Auth::user()->username;
        $update->save();
        if($request->RECORD_STATUS == 'N'){
            return back()->with('alert-class','success')->with('alert','ออนไลน์เนื้อหาแล้ว');
        }else{
            return back()->with('alert-class','danger')->with('alert','ออฟไลน์เนื้อหาแล้ว');
        }
    }
}
