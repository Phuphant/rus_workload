<?php

namespace App\Http\Controllers;


use App\inv_chart;
use App\sys_menu;
use Illuminate\Http\Request;

class ChartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['menu'] = sys_menu::where('menu_in','main')
                        ->where('record_status','N')        
                        ->orderby('menu_order')
                        ->get();
        $data['sub_menu'] = sys_menu::where('menu_in','<>','main')
                        ->where('record_status','N')        
                        ->orderby('menu_order')
                        ->get(); 
        
        $data['chart'] = inv_chart::where('system_delete','none')
                        ->orderby('id')                        
                        ->paginate(20);

        return view('chart.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['menu'] = sys_menu::where('menu_in','main')
                        ->where('record_status','N')        
                        ->orderby('menu_order')
                        ->get();
        $data['sub_menu'] = sys_menu::where('menu_in','<>','main')
                        ->where('record_status','N')        
                        ->orderby('menu_order')
                        ->get();                 

        return view('chart.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = inv_chart::max('id');
        $id = $id+1;

        $chart = new inv_chart();
        $chart->id               = $id;
        $chart->CHART_CODE       = $request->CHART_CODE;
        $chart->CHART_NAME_TH    = $request->CHART_NAME_TH;
        $chart->CHART_NAME_EN    = $request->CHART_NAME_EN;
        $chart->RECORD_STATUS    = $request->RECORD_STATUS;
        $chart->CHART_DESC       = $request->CHART_DESC;        
        $chart->system_delete    = 'none';

        #return $chart;
        $chart->save();
        return redirect()->action('ChartController@index')->with('alert','บันทึกข้อมูลเรียบร้อยแล้ว');   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['menu'] = sys_menu::where('menu_in','main')
                        ->where('record_status','N')        
                        ->orderby('menu_order')
                        ->get();
        $data['sub_menu'] = sys_menu::where('menu_in','<>','main')
                        ->where('record_status','N')        
                        ->orderby('menu_order')
                        ->get(); 
        
        $data['chart'] = inv_chart::where('id',$id)
                        ->get();

        return view('chart.edit',$data);  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $chart = inv_chart::find($id);
        $chart->CHART_CODE       = $request->CHART_CODE;
        $chart->CHART_NAME_TH    = $request->CHART_NAME_TH;
        $chart->CHART_NAME_EN    = $request->CHART_NAME_EN;
        $chart->CHART_DESC       = $request->CHART_DESC;        
        $chart->RECORD_STATUS    = $request->RECORD_STATUS;

        $chart->save();
        return redirect()->action('ChartController@index')->with('alert','บันทึกข้อมูลเรียบร้อยแล้ว');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $update = inv_chart::find($id);
        $update->system_delete = "delete";        
        $update->save();  
        return back()->with('alert-class','success')->with('alert','ลบข้อมูลแล้ว');
        #return "ddddd";
    }

    public function change_status(Request $request)
    {
        // return "change_status : ".$id;
        $update = inv_chart::find($request->id);
        $update->RECORD_STATUS = $request->RECORD_STATUS;
        #$update->updated_by = Auth::user()->username;
        $update->save();
        if($request->RECORD_STATUS == 'N'){
            return back()->with('alert-class','success')->with('alert','ออนไลน์เนื้อหาแล้ว');
        }else{
            return back()->with('alert-class','danger')->with('alert','ออฟไลน์เนื้อหาแล้ว');
        }
    }
}
