<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class vw_his_person extends Model
{
    #protected $connection = "sqlsrv";    
    protected $connection = "sqlsrv_hrd";    
    protected $table = "vw_his_person";

    public function getFullName()
    {
        return "{$this->title_name_th}{$this->fname_th} {$this->lname_th}";
    }    
    
    public function permissions()
    {
        #return $this->setConnection('sqlsrv')->hasMany('App\permissions','person_id','id_card');
        return $this->setConnection('mysql')->hasMany('App\vw_permissions','person_id','id_card');
    }
    
    public function user_profile()
    {
        return $this->setConnection('mysql')->hasOne('App\User','id_card','id_card');
    }
    /**/
}


