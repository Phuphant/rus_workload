<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class slip_match_per_expend extends Model
{
    protected $connection = "sqlsrv";
    protected $table = "slip_match_per_expend";    

    public function vw_his_person()
    {
        return $this->hasone('App\vw_his_person','person_id','person_id')->where('statuslist',9)->where('record_status','N');
    }
}

