<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function vw_his_person()
    {
        return $this->setConnection('sqlsrv_hrd')->hasOne('App\vw_his_person', 'id_card', 'id_card');
    }
    
    public function permissions()
    {
        #return $this->hasone('App\permissions','person_id','id_card')->where('role_id','<>','0');
        return $this->hasMany('App\vw_permissions','person_id','id_card')->where('record_status','N');
    }

    
}
