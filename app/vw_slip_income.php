<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class vw_slip_income extends Model
{
    protected $connection = "sqlsrv"; 
    protected $table = "vw_slip_income";
}
