<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class vw_permissions extends Model
{
    protected $table = "vw_permissions";

    public function role()
    {
        return $this->belongto('App\roles','role_id','id')->where('role_id','<>','0');
    }
}
