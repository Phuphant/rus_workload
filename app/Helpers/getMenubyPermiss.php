<?php

use App\sys_menu;
use App\roles;

if (!function_exists('getMainMenuByPermis')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function getMainMenuByPermis($role_id)
    {
        #return $role_id;
        $role = roles::select('can_access')->where('id',$role_id)->first();
        #return $role->can_access;
        $menu_array = json_decode($role->can_access);
        // $menu_array = explode('/',$role->can_access);
        // $count = count($menu_array);
        // dd($menu_array);
        $main_menu = sys_menu::where('menu_in','main')                        
                        ->where('record_status','N')     
                        ->where('system_delete','none')  
                        ->whereIn('id',$menu_array)   
                        ->orderby('menu_order')
                        ->get();

        return $main_menu;
    }
}

if (!function_exists('getSubMenuByPermis')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function getSubMenuByPermis($role_id)
    {
        $role = roles::select('can_access')->where('id',$role_id)->first();
        #return $role->can_access;
        $sub_menu_array = json_decode($role->can_access);
        // $menu_array = explode('/',$role->can_access);
        // $count = count($menu_array);
        // dd($menu_array);
        #return $sub_menu_array;

        $sub_menu = sys_menu::where('menu_in','<>','main')                        
                        ->where('record_status','N')     
                        ->where('system_delete','none')  
                        ->whereIn('menu_in',$sub_menu_array)   
                        ->orderby('menu_order')
                        ->get();

        return $sub_menu;
    }
}



if (!function_exists('getMenuName')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function getMenuName($id)
    {
        $menu = sys_menu::where('record_status','N')->where('id',$id)->get();
        return $menu;
    }
}
