<?php

use App\sys_menu;
use App\roles;
use App\vw_permissions;


if (!function_exists('getPermissbyUser')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function getPermissbyUser($id)
    {
        $permis =   vw_permissions::where('record_status','N')
                            ->where('person_id',$id)       
                            ->orderby('id')                      
                            ->get();

        return $permis;
    }
}

